'''
难度 中等
题目链接 https://leetcode.cn/problems/group-anagrams/description/
'''

from collections import defaultdict
from typing import List

class Solution:
    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:
        结果 = defaultdict(list)
        for s in strs:
            新排序后的字符串 = '' . join(sorted(s))
            结果[新排序后的字符串].append(s)

        return list(结果.values())
    

if __name__ == '__main__':
    s = Solution()
    参数 = [
        [["eat", "tea", "tan", "ate", "nat", "bat"]],
        [[""]],
        [["a"]],
    ]
    预期 = [
        [["bat"],["nat","tan"],["ate","eat","tea"]],
        [[""]],
        [["a"]],
    ]
    for k,v in enumerate(参数):
        r = s.groupAnagrams(v[0])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))