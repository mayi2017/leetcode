'''
难度 简单
标签 二分查找
题目链接 https://leetcode.cn/problems/binary-search/description/
'''

from typing import List

class Solution:
    def hIndex(self, citations: List[int]) -> int:
        # 从小到大排序
        citations.sort()

        n = len(citations)
        for i in range(n):
            # 当前对应的值 如果大于 对应的论文数量 则后面都满足条件
            if citations[i] >= n - i:
                return n - i

        return 0

if __name__ == '__main__':
    s = Solution()
    参数 = [
        [[3,0,6,1,5]],
        [[1,3,1]],
    ]
    预期 = [
        3,
        1,
    ]
    for k,v in enumerate(参数):
        r = s.hIndex(v[0])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))