'''
难度 中等
题目链接 https://leetcode.cn/problems/rotate-image/description/
'''

from typing import List

class Solution:
    # 直接遍历
    def setZeroes(self, matrix: List[List[int]]) -> None:
        行 = []
        列 = []
        总行数 = len(matrix)
        总列数 = len(matrix[0])
        # 先找到出现0的行和列
        for i in range(总行数):
            for j in range(总列数):
                if matrix[i][j] == 0:
                    行.append(i)
                    列.append(j)
        
        # 行置零
        for i in set(行):
            for j in range(总列数):
                matrix[i][j] = 0
        # 列置零
        for i in range(总行数):
            for j in set(列):
                matrix[i][j] = 0
        
        # print(matrix)

    

if __name__ == '__main__':
    s = Solution()
    参数 = [
        [[
            [1,1,1],
            [1,0,1],
            [1,1,1]
        ]],
        [[
            [0,1,2,0],
            [3,4,5,2],
            [1,3,1,5]
        ]],
    ]
    预期 = [
        [
            [1,0,1],
            [0,0,0],
            [1,0,1]
        ],
        [
            [0,0,0,0],
            [0,4,5,0],
            [0,3,1,0]
        ],
    ]
    for k,v in enumerate(参数):
        r = s.setZeroes(v[0])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))