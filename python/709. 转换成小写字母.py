'''
难度 简单
题目链接 https://leetcode.cn/problems/to-lower-case/description/
'''

class Solution:

    # 使用内置函数
    def toLowerCase1(self, s: str) -> str:
        return s.lower()
    
    # 使用ASCII实现
    def toLowerCase2(self, s: str) -> str:
        结果 = ''
        for char in s:
            ascii = ord(char)
            if 65 <= ascii <= 90:
                结果 += chr(ascii + 32)
            else:
                结果 += char

        return 结果
    
    # 使用ASCII实现 优化
    def toLowerCase(self, s: str) -> str:
        '''
        大写变小写、小写变大写：字符 ^= 32 （大写 ^= 32 相当于 +32，小写 ^= 32 相当于 -32）
        大写变小写、小写变小写：字符 |= 32 （大写 |= 32 就相当于+32，小写 |= 32 不变）
        大写变大写、小写变大写：字符 &= -33 （大写 &= -33 不变，小写 &= -33 相当于 -32）
        因为ASCII的第三个bit位不同 对应的就是32
        所以：异或就是大小写切换 与就是都变小写 并就是都变大写
        '''
        结果 = ''
        for char in s:
            ascii = ord(char)
            if 65 <= ascii <= 90 or 97 <= ascii <= 122:
                结果 += chr(ascii | 32)
            else:
                结果 += char

        return 结果


if __name__ == '__main__':
    s = Solution()
    参数 = [
        ["Hello"],
        ["here"],
        ["LOVELY"],
    ]
    预期 = [
        'hello',
        'here',
        'lovely',
    ]
    for k,v in enumerate(参数):
        r = s.toLowerCase(v[0])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))