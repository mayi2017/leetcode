'''
难度 简单
题目链接 https://leetcode.cn/problems/count-the-digits-that-divide-a-number/description/
'''

class Solution:
    # 存列表
    def countDigits1(self, num: int) -> int:
        可以整除的数字 = []
        numStr = str(num)
        for n in range(len(numStr)):
            if int(numStr[n]) != 0 and num % int(numStr[n]) == 0:
                可以整除的数字.append(numStr[n])
 
        return len(可以整除的数字)
    
    # 记数
    def countDigits2(self, num: int) -> int:
        结果 = 0
        numStr = str(num)
        for n in range(len(numStr)):
            if int(numStr[n]) != 0 and num % int(numStr[n]) == 0:
                结果 += 1
 
        return 结果
    
    # 记数 减少空间占用
    def countDigits(self, num: int) -> int:
        结果 = 0
        temp = num
        while temp:
            结果 += (num % (temp % 10) == 0)
            temp //= 10
 
        return 结果
    

if __name__ == '__main__':
    s = Solution()
    参数 = [
        [7],
        [121],
        [1248],
    ]
    预期 = [
        1,
        2,
        4,
    ]
    for k,v in enumerate(参数):
        r = s.countDigits(v[0])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))