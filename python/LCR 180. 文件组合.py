'''
难度 简单
标签 数组
题目链接 https://leetcode.cn/problems/he-wei-sde-lian-xu-zheng-shu-xu-lie-lcof/description/
'''

from typing import List


class Solution:
    # 轮询文件个数
    def fileCombination1(self, target: int) -> List[List[int]]:
        结果集合 = []

        # 连续文件个数
        for i in range(2, target + 1):
            # 文件个数为奇数 平均值一定是最中间的那个
            if i % 2 == 1:
                # 如果平均值不为整数 那一定不存在这种集合
                if target % i == 0 and int(target / i - i // 2) > 0:
                    结果集合.insert(0, 
                        [
                            i for i in range(
                                int(target / i - i // 2), 
                                int(target / i + i // 2 + 1)
                            )
                        ]
                    )
            # 文件个数为偶数 平均值一定是最中间的那2个
            else:
                # 最中间的那2个数的和必须为整数 且必须是奇数 并且中间数的和一定大于数量的2倍 不然左边会到负数
                if target * 2 % i == 0 and target * 2 / i % 2 == 1 and int((target * 2 / i) // 2 - i // 2 + 1) > 0:
                    结果集合.insert(0, 
                        [
                            i for i in range(
                                int((target * 2 / i) // 2 - i // 2 + 1), 
                                int((target * 2 / i) // 2 + i // 2 + 1)
                            )
                        ]
                    )
        
        return 结果集合

    # 滑动窗口
    def fileCombination(self, target: int) -> List[List[int]]:
        左, 右, 和, 结果集合 = 1, 2, 3, []
        
        # 因为文件数至少为2
        while 左 < 右:  
            # 和 比 目标值大 则把左边的指针右移 缩小和
            # 此时包含 = 是因为相等时 也要进行左移 否则死循环一直在上面if
            if 和 >= target:
                if 和 == target:
                    结果集合.append(list(range(左, 右 + 1)))

                # 注意这里的细节 左边右移 则需要先减去 左再自增
                和 -= 左
                左 += 1
            # 和 比 目标值小 则把右边的指针右移 增大和
            else:
                # 注意这里的细节 右边右移 则需要先自增 和再加
                右 += 1
                和 += 右
        
        return 结果集合
    


    

if __name__ == '__main__':
    s = Solution()
    参数 = [
        [12],
        [18],
    ]
    预期 = [
        [[3, 4, 5]],
        [[3,4,5,6],[5,6,7]],
    ]
    for k,v in enumerate(参数):
        r = s.fileCombination(v[0])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))