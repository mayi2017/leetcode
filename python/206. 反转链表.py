'''
难度 简单
题目链接 https://leetcode.cn/problems/reverse-linked-list/description/
'''

class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


from typing import Optional

class Solution:
    def reverseList(self, head: Optional[ListNode]) -> Optional[ListNode]:
        prev = None
        curr = head

        while curr != None:
            next = curr.next
            # 改变指向
            curr.next = prev
            # 下一轮的值
            prev = curr
            curr = next
        
        # 最后一次循环 curr已经为None 所以直接返回 prev
        return prev
    

if __name__ == '__main__':
    s = Solution()
    参数 = [
        [10],
        [37],
    ]
    预期 = [
        182,
        1478,
    ]
    for k,v in enumerate(参数):
        r = s.reverseList(v[0])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))