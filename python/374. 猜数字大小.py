'''
难度 简单
标签 二分查找
题目链接 https://leetcode.cn/problems/guess-number-higher-or-lower/description/
'''


# The guess API is already defined for you.
# @param num, your guess
# @return -1 if num is higher than the picked number
#          1 if num is lower than the picked number
#          otherwise return 0
# def guess(num: int) -> int:

class Solution:
    def guessNumber(self, n: int) -> int:
        最小, 最大 = 1, n
        while 最小 <= 最大:
            中间 = 最小 + (最大 - 最小) // 2
            猜测结果 = guess(中间)
            if 猜测结果 == 0:
                return 中间
            elif 猜测结果 == -1:
                最大 = 中间 - 1
            else:
                最小 = 中间 + 1

if __name__ == '__main__':
    s = Solution()
    参数 = [
        [10, 6],
        [1, 1],
        [2, 1],
        [2, 2],
    ]
    预期 = [
        6,
        1,
        1,
        2,
    ]
    for k,v in enumerate(参数):
        r = s.guessNumber(v[0])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))