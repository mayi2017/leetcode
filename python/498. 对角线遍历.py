'''
难度 中等
题目链接 https://leetcode.cn/problems/diagonal-traverse/description/
'''

from typing import List

class Solution:
    def findDiagonalOrder(self, mat: List[List[int]]) -> List[int]:
        # 处理特殊为空的情况
        if not mat:
            return []
        
        总行数 = len(mat)
        总列数 = len(mat[0])
        对角线的总数量 = 总行数 + 总列数 - 1
        结果 = []

        # 对角线上的数的坐标 x+y=i 
        for i in range(对角线的总数量):
            # 对角线偶数时 自下而上
            if i % 2 == 0:
                行左界, 行右界 = 0, i
                for h in range(行左界, 行右界+1):
                    结果.append(mat[h][i-h])
                pass
            # 对角线奇数时 自上而下
            else:
                行左界, 行右界 = 0, i
                for h in range(行左界, 行右界+1):
                    结果.append(mat[h][i-h])
                pass

        return 结果

if __name__ == '__main__':
    s = Solution()
    参数 = [
        [[[1,2,3],[4,5,6],[7,8,9]]],
        [[[1,2],[3,4]]],
    ]
    预期 = [
        [1,2,4,7,5,3,6,8,9],
        [1,2,3,4],
    ]
    for k,v in enumerate(参数):
        r = s.findDiagonalOrder(v[0])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))