'''
难度 简单
题目链接 https://leetcode.cn/problems/keyboard-row/description/
'''

class Solution:
    def 获取字母字典(self):
        return {
            'q':'1',
            'w':'1',
            'e':'1',
            'r':'1',
            't':'1',
            'y':'1',
            'u':'1',
            'i':'1',
            'o':'1',
            'p':'1',
            'a':'2',
            's':'2',
            'd':'2',
            'f':'2',
            'g':'2',
            'h':'2',
            'j':'2',
            'k':'2',
            'l':'2',
            'z':'3',
            'x':'3',
            'c':'3',
            'v':'3',
            'b':'3',
            'n':'3',
            'm':'3'
        }

    def findWords1(self, words):
        字母字典 = self.获取字母字典()
        结果 = []
        for w in words:
            if len(w) <= 1:
                结果.append(w)
                continue
            首字母对应行 = 字母字典.get(w[0].lower())
            单词符合 = True
            for i in range(1, len(w)):
                if 字母字典.get(w[i].lower()) != 首字母对应行:
                    单词符合 = False
                    break
            单词符合 and 结果.append(w)
        
        return 结果

    def findWords(self, words):
        # 转为集合
        字母字典 = [set("qwertyuiop"), set("asdfghjkl"), set("zxcvbnm")]

        '''
        [x for x in iterable if condition] 列表解析式，它正在遍历一个可迭代对象（iterable）中的每个元素x，如果满足某个条件（condition），就将x添加到新的列表中。
        any(iterable) 用于检查给定的可迭代对象是否包含任何满足指定条件的元素。
        set(word.lower()) 单词转为小写后 转为字母集合（字母不重复）
        set(word.lower()).issubset(行) 字母集合是否为行的子集 返回True False
        '''
        return [word for word in words if any(
            set(word.lower()).issubset(行) for 行 in 字母字典
        )]


if __name__ == '__main__':
    s = Solution()
    参数 = [
        [["Hello","Alaska","Dad","Peace"]],
        # [["omk"]],
        # [["adsdf","sfd"]],
    ]
    预期 = [
        ["Alaska","Dad"],
        [],
        ["adsdf","sfd"],
    ]
    for k,v in enumerate(参数):
        r = s.findWords(v[0])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))