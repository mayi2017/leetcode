'''
难度 简单
标签 链表
题目链接 https://leetcode.cn/problems/cong-wei-dao-tou-da-yin-lian-biao-lcof/description/
'''

from typing import List, Optional

class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    # 递归
    def reverseBookList1(self, head: Optional[ListNode]) -> List[int]:
        if head:
            return self.reverseBookList(head.next) + [head.val]
        else:
            return []
    
    # 递归 if 换种写法
    def reverseBookList2(self, head: Optional[ListNode]) -> List[int]:

        return self.reverseBookList(head.next) + [head.val] if head else []
    
    # 利用栈
    def reverseBookList(self, head: Optional[ListNode]) -> List[int]:
        stack = []
        while head:
            # 顺序存
            stack.append(head.val)
            head = head.next

        # 反转输出
        return stack[::-1]

if __name__ == '__main__':
    s = Solution()
    参数 = [
        [[3,6,4,1]],
    ]
    预期 = [
        [1,4,6,3],
    ]
    for k,v in enumerate(参数):
        r = s.reverseBookList(v[0])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))