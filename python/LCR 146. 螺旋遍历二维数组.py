'''
难度 简单
标签 数组
题目链接 https://leetcode.cn/problems/shun-shi-zhen-da-yin-ju-zhen-lcof/description/
'''

from typing import List


class Solution:
    # 按照要求方向走 上下左右边界不断收缩
    def spiralArray(self, array: List[List[int]]) -> List[int]:
        # 特殊处理空值
        if not array:
            return []
        
        # 初始化 上下左右的边界 
        上, 下, 左, 右, 结果 = 0, len(array) - 1, 0, len(array[0]) - 1, []

        # 开始走起来
        while True:
            # 从左到右 上++ 
            for i in range(左, 右 + 1):
                结果.append(array[上][i])
            上 += 1
            if 上 > 下: break

            # 从上到下 右-- 
            for i in range(上, 下 + 1):
                结果.append(array[i][右])
            右 -= 1
            if 左 > 右: break

            # 从右到左 下-- 
            for i in range(右, 左 - 1, -1):
                结果.append(array[下][i])
            下 -= 1
            if 上 > 下: break

            # 从下到上 左++ 
            for i in range(下, 上 - 1, -1):
                结果.append(array[i][左])
            左 += 1
            if 左 > 右: break

        return 结果
    

if __name__ == '__main__':
    s = Solution()
    参数 = [
        [[[1,2,3],[8,9,4],[7,6,5]]],
        [[[1,2,3,4],[12,13,14,5],[11,16,15,6],[10,9,8,7]]],
    ]
    预期 = [
        [1,2,3,4,5,6,7,8,9],
        [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],
    ]
    for k,v in enumerate(参数):
        r = s.spiralArray(v[0])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))