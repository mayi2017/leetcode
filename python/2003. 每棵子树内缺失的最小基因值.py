'''
难度 困难
标签 树
题目链接 https://leetcode.cn/problems/smallest-missing-genetic-value-in-each-subtree/description/
'''

from typing import List


class Solution:
    # 
    def smallestMissingValueSubtree(self, parents: List[int], nums: List[int]) -> List[int]:
        结果 = []
        
        

        return 结果

    

if __name__ == '__main__':
    s = Solution()
    参数 = [
        [[-1,0,0,2], [1,2,3,4]],
        [[-1,0,1,0,3,3], [5,4,6,2,1,3]],
        [[-1,2,3,0,2,4,1], [2,3,4,5,6,7,8]],
    ]
    预期 = [
        [5,1,1,1],
        [7,1,1,4,2,1],
        [1,1,1,1,1,1,1],
    ]
    for k,v in enumerate(参数):
        r = s.smallestMissingValueSubtree(v[0], v[1])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))