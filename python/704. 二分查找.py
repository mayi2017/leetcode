'''
难度 简单
标签 二分查找
题目链接 https://leetcode.cn/problems/binary-search/description/
'''

from typing import List

class Solution:
    def search(self, nums: List[int], target: int) -> int:
        头,尾 = 0,len(nums) - 1
        
        while 头 <= 尾:
            中 = (头 + 尾) // 2
            if nums[中] < target:
                头 = 中 + 1
            elif nums[中] > target:
                尾 = 中 - 1
            else:
                return 中
        
        return -1

if __name__ == '__main__':
    s = Solution()
    参数 = [
        [[-1,0,3,5,9,12], 9],
        [[-1,0,3,5,9,12], 2],
    ]
    预期 = [
        4,
        -1,
    ]
    for k,v in enumerate(参数):
        r = s.search(v[0], v[1])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))