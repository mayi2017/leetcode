'''
难度 简单
题目链接 https://leetcode.cn/problems/two-sum/description/
'''

from typing import List

class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        for i in range(len(nums)):
            try:
                第二个数下标 = nums[(i+1):].index(target - nums[i]) + i + 1
                break
            except Exception as e:
                continue
            finally:
                pass

        return [i, 第二个数下标]

if __name__ == '__main__':
    s = Solution()
    参数 = [
        [[2,7,11,15], 9],
        [[3,2,4], 6],
        [[3,3], 6],
    ]
    预期 = [
        [0,1],
        [1,2],
        [0,1],
    ]
    for k,v in enumerate(参数):
        r = s.twoSum(v[0], v[1])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))