'''
难度 中等
题目链接 https://leetcode.cn/problems/3sum/description/
'''

from collections import defaultdict
from typing import List

class Solution:
    def threeSum(self, nums: List[int]) -> List[List[int]]:
        结果 = []
        n = len(nums)
        # 处理特殊情况
        if not nums or n < 3:
            return []
        
        nums.sort()
        # 第一个元素 轮询
        for i in range(n):
            # 第一个元素就大于 0 那就可以直接返回[]了
            if nums[i] > 0:
                return 结果
            # 每一轮都初始化左右指针
            左指针 = i + 1
            右指针 = n - 1
            

            
        pass
    

if __name__ == '__main__':
    s = Solution()
    参数 = [
        [[-1,0,1,2,-1,-4]],
        [[0,1,1]],
        [[0,0,0]],
    ]
    预期 = [
        [[-1,-1,2],[-1,0,1]],
        [],
        [0,0,0],
    ]
    for k,v in enumerate(参数):
        r = s.threeSum(v[0])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))