'''
难度 简单
题目链接 https://leetcode.cn/problems/find-the-middle-index-in-array/description/
'''

from typing import List

class Solution:
    # 常规循环
    def findMiddleIndex(self, nums: List[int]) -> int:
        列表总和 = sum(nums)
        左边和 = 0
        if len(nums) <= 1:
            return 0

        for i in range(0, len(nums)):
            if 列表总和 - nums[i] == 左边和 * 2:
                # print(nums, i, 左边和, nums[i])
                return i
            左边和 += nums[i]

        return -1

if __name__ == '__main__':
    s = Solution()
    参数 = [
        [[2,3,-1,8,4]],
        [[1,-1,4]],
        [[2,5]],
        [[1]],
    ]
    预期 = [
        3,
        2,
        -1,
        0,
    ]
    for k,v in enumerate(参数):
        r = s.findMiddleIndex(v[0])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))