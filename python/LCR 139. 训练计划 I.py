'''
难度 简单
标签 数组
题目链接 https://leetcode.cn/problems/diao-zheng-shu-zu-shun-xu-shi-qi-shu-wei-yu-ou-shu-qian-mian-lcof/description/
'''

from typing import List


class Solution:
    # 分两个数组
    def trainingPlan1(self, actions: List[int]) -> List[int]:
        # 定义两个数组
        left = []
        right = []

        for i in actions:
            if i % 2 == 1:
                left.append(i)
            else:
                right.append(i)
        
        # 合并返回
        return left + right
    
    # 双指针 交换
    def trainingPlan(self, actions: List[int]) -> List[int]:
        # 初始化双指针
        i, j = 0, len(actions) - 1
        while i < j:
            # 头指针 遇见奇数 继续走
            if actions[i] % 2 == 1:
                i += 1
                continue
            # 尾指针 遇见偶数 继续走
            if actions[j] % 2 == 0:
                j -= 1
                continue
            
            # 交换头尾指针的值
            actions[i], actions[j] =  actions[j], actions[i]
        
        return actions
            
    

if __name__ == '__main__':
    s = Solution()
    参数 = [
        [[1,2,3,4,5]],
    ]
    预期 = [
        [1,3,5,2,4],
    ]
    for k,v in enumerate(参数):
        r = s.trainingPlan(v[0])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))