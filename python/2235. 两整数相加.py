'''
难度 简单
题目链接 https://leetcode.cn/problems/add-two-integers/description/
'''

class Solution:
    def sum(self, num1: int, num2: int) -> int:
        return num1 + num2

    

if __name__ == '__main__':
    s = Solution()
    参数 = [
        [12, 5],
        [-10, 4],
    ]
    预期 = [
        17,
        -6,
    ]
    for k,v in enumerate(参数):
        r = s.sum(v[0], v[1])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))