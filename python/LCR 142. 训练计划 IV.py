'''
难度 简单
标签 链表
题目链接 https://leetcode.cn/problems/he-bing-liang-ge-pai-xu-de-lian-biao-lcof/description/
'''

from typing import List, Optional

class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def trainningPlan(self, l1: Optional[ListNode], l2: Optional[ListNode]) -> Optional[ListNode]:
        # 因为两个链表合并 由于初始状态合并链表中无节点 所以增加伪头结点
        tmp = curr = ListNode(0)

        while l1 and l2:
            if l1.val < l2.val:
                curr.next = l1
                l1 = l1.next
            else:
                curr.next = l2
                l2 = l2.next

            # 合并链表指针后移
            curr = curr.next

        # 停止循环 意味着 有一个链表走到头了
        curr.next = l1 if l1 else l2 

        # 凑的伪头结点的下一个
        return tmp.next
    

if __name__ == '__main__':
    s = Solution()
    参数 = [
        [[4,5,1,9], 5],
        [[4,5,1,9], 1],
    ]
    预期 = [
        [4,1,9],
        [4,5,9],
    ]
    for k,v in enumerate(参数):
        r = s.reverseBookList(v[0])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))