'''
难度 简单
标签 链表
题目链接 https://leetcode.cn/problems/fan-zhuan-lian-biao-lcof/description/
'''

from typing import List, Optional

class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def trainningPlan(self, head: Optional[ListNode]) -> Optional[ListNode]:
        # 判空
        if head == None:
            return head

        pre, curr = None, head
        while curr:
            next = curr.next
            # 改变指向
            curr.next = pre

            pre = curr
            curr = next
        
        return pre
    

if __name__ == '__main__':
    s = Solution()
    参数 = [
        [[4,5,1,9], 5],
        [[4,5,1,9], 1],
    ]
    预期 = [
        [4,1,9],
        [4,5,9],
    ]
    for k,v in enumerate(参数):
        r = s.reverseBookList(v[0])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))