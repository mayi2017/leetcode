'''
难度 中等
题目链接 https://leetcode.cn/problems/longest-consecutive-sequence/description/
'''

from typing import List

class Solution:
    def longestConsecutive(self, nums: List[int]) -> int:
        最长连续序列数 = 0
        数字集合 = set(nums)

        for n in nums:
            # 当 前一个数不在集合中 才是连续序列的起始
            if n - 1 not in 数字集合:
                当前数字 = n
                当前连续序列数 = 1
            
                while 当前数字 + 1 in 数字集合:
                    当前数字 += 1
                    当前连续序列数 += 1
                
                最长连续序列数 = max(最长连续序列数, 当前连续序列数)

        return 最长连续序列数

if __name__ == '__main__':
    s = Solution()
    参数 = [
        [[100,4,200,1,3,2]],
        [[0,3,7,2,5,8,4,6,0,1]],
    ]
    预期 = [
        4,
        9,
    ]
    for k,v in enumerate(参数):
        r = s.longestConsecutive(v[0])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))