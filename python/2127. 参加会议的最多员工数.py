'''
难度 困难
标签 数组
题目链接 https://leetcode.cn/problems/maximum-employees-to-be-invited-to-a-meeting/description/
'''

from typing import List


class Solution:
    # 假设那个人是必须去的 统计出能参加多少 拿最大的参会人数
    def maximumInvitations(self, favorite: List[int]) -> int:
        结果 = 0
        # 总人数
        n = len(favorite)

        # 一个个遍历必须参加的人
        for i in range(n):
            参与会议的人 = []
            参与会议的人.append(i)
            j = i
            # 当喜欢的人还没坐下来 或者 喜欢的人就是 第一个 则条件还可以继续
            while favorite[j] not in 参与会议的人:
                参与会议的人.append(favorite[j])
                j = favorite[j]
            print(n, i, 参与会议的人)
            # 最后一个人应该满足的条件 他喜欢的人要么是第一个人 要么是他前面的那个
            if favorite[参与会议的人[-1]] == 参与会议的人[0] or favorite[参与会议的人[-1]] == 参与会议的人[-2]:
                结果 = max(结果, len(参与会议的人))
                print(n, i, 参与会议的人)

            # 如果已经能坐下所有人 后面的就没必要循环了
            if 结果 == n: break  

        return 结果

    

if __name__ == '__main__':
    s = Solution()
    参数 = [
        # [[2,2,1,2]],
        # [[1,2,0]],
        # [[3,0,1,4,1]],
        [[1,0,0,2,1,4,7,8,9,6,7,10,8]],
    ]
    预期 = [
        # 3,
        # 3,
        # 4,
        6,
    ]
    for k,v in enumerate(参数):
        r = s.maximumInvitations(v[0])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))