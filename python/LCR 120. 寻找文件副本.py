'''
难度 简单
标签 数组
题目链接 https://leetcode.cn/problems/shu-zu-zhong-zhong-fu-de-shu-zi-lcof/description/
'''

from typing import List


class Solution:
    # 利用 hashmap
    def findRepeatDocument1(self, documents: List[int]) -> int:
        文件映射 = {}
        
        for i in range(len(documents)):
            if 文件映射.get(documents[i]):
                return documents[i]
            文件映射[documents[i]] = 1

        return -1
    
    # 利用 hashmap 2
    def findRepeatDocument(self, documents: List[int]) -> int:
        文件映射 = set()
        
        for i in documents:
            if i in 文件映射:
                return i
            文件映射.add(i)

        return -1


    

if __name__ == '__main__':
    s = Solution()
    参数 = [
        [[2, 5, 3, 0, 5, 0]],
    ]
    预期 = [
        0,
    ]
    for k,v in enumerate(参数):
        r = s.findRepeatDocument(v[0])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))