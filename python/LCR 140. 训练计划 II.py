'''
难度 简单
标签 链表
题目链接 https://leetcode.cn/problems/lian-biao-zhong-dao-shu-di-kge-jie-dian-lcof/description/
'''

from typing import List, Optional

class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def trainingPlan1(self, head: Optional[ListNode], cnt: int) -> Optional[ListNode]:
        # 设置快慢指针 快指针先走cnt步 然后快慢同时走 等快指针到null了 慢指针所在的位置 即为答案
        slow, fast = head, head
        # 快指针先走cnt步 
        for _ in range(cnt):
            fast = fast.next 

        # 快慢指针一起走 
        while fast:
            slow, fast = slow.next, fast.next 
        
        return slow

    def trainingPlan(self, head: Optional[ListNode], cnt: int) -> Optional[ListNode]:
        # 设置快慢指针 快指针先走cnt步 然后快慢同时走 等快指针到null了 慢指针所在的位置 即为答案
        slow, fast = head, head

        i = 0
        while fast:
            i += 1
            # 快的先走cnt步
            if i <= cnt:
                fast = fast.next 
                continue
            slow, fast = slow.next, fast.next 
        
        return slow
    

if __name__ == '__main__':
    s = Solution()
    参数 = [
        [[4,5,1,9], 5],
        [[4,5,1,9], 1],
    ]
    预期 = [
        [4,1,9],
        [4,5,9],
    ]
    for k,v in enumerate(参数):
        r = s.reverseBookList(v[0])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))