'''
难度 简单
标签 链表
题目链接 https://leetcode.cn/problems/shan-chu-lian-biao-de-jie-dian-lcof/description/
'''

from typing import List, Optional

class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    # 双指针
    def deleteNode1(self, head: ListNode, val: int) -> ListNode:
        # 当需要删除的是头结点时，直接返回 head.next 
        if head.val == val:
            return head.next
        
        # 初始化前节点 当前节点
        pre, curr = head, head.next

        while curr and curr.val != val:
            pre, curr = curr, curr.next
        
        if curr:
            pre.next = curr.next
        
        return head

    # 单指针
    def deleteNode(self, head: ListNode, val: int) -> ListNode:
        # 当需要删除的是头结点时，直接返回 head.next 
        if head.val == val:
            return head.next
        
        # 当前节点
        curr = head

        while curr.next and curr.next.val != val:
            curr = curr.next
        
        if curr.next:
            curr.next = curr.next.next
        
        return head
    

if __name__ == '__main__':
    s = Solution()
    参数 = [
        [[4,5,1,9], 5],
        [[4,5,1,9], 1],
    ]
    预期 = [
        [4,1,9],
        [4,5,9],
    ]
    for k,v in enumerate(参数):
        r = s.reverseBookList(v[0])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))