'''
难度 简单
标签 链表
题目链接 https://leetcode.cn/problems/liang-ge-lian-biao-de-di-yi-ge-gong-gong-jie-dian-lcof/description/
'''

class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def getIntersectionNode(self, headA: ListNode, headB: ListNode) -> ListNode:
        A, B = headA, headB
        # 各自走过对方走过的路 到再相遇时 双方都走过一样多的路 剩下的就是共同的路
        while A != B:
            A = A.next if A else headB
            B = B.next if B else headA
        
        return A
    

if __name__ == '__main__':
    s = Solution()
    参数 = [
        [[4,5,1,9], 5],
        [[4,5,1,9], 1],
    ]
    预期 = [
        [4,1,9],
        [4,5,9],
    ]
    for k,v in enumerate(参数):
        r = s.getIntersectionNode(v[0])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))