'''
难度 简单
标签 数组
题目链接 https://leetcode.cn/problems/gou-jian-cheng-ji-shu-zu-lcof/description/
'''

from typing import List


class Solution:
    # 算出总乘积 再循环输出
    def statisticalResult(self, arrayA: List[int]) -> List[int]:
        总乘积 = 1
        含零映射 = {}
        结果 = []
        
        # 得到非0的乘积 含0的下标map
        for i in range(len(arrayA)):
            if arrayA[i] == 0:
               含零映射[i] = 1
            else: 
                总乘积 *= arrayA[i]

        for i in range(len(arrayA)):
            if arrayA[i] == 0:
                if len(含零映射) > 1:
                    结果.append(0)
                else:
                    结果.append(总乘积)
            else:
                if len(含零映射) > 0:
                    结果.append(0)
                else:
                    结果.append(int(总乘积 / arrayA[i]))

        return 结果

    

if __name__ == '__main__':
    s = Solution()
    参数 = [
        [[2, 4, 6, 8, 10]],
    ]
    预期 = [
        [1920, 960, 640, 480, 384],
    ]
    for k,v in enumerate(参数):
        r = s.statisticalResult(v[0])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))