'''
难度 简单
题目链接 https://leetcode.cn/problems/number-of-segments-in-a-string/description/
'''

class Solution:
    def countSegments(self, s: str) -> int:
        s = s + ' '
        单词 = ''
        单词数量 = 0
        for i in range(0, len(s)):
            if s[i] == ' ' and 单词 != '':
                单词数量 += 1
                单词 = ''
            elif s[i] != ' ':
                单词 += s[i]
        
        return 单词数量


if __name__ == '__main__':
    s = Solution()
    参数 = [
        ["Hello, my name is John"],
        ["Hello"],
        [""],
        [", , , ,        a, eaefa"],
    ]
    预期 = [
        5,
        1,
        0,
        6,
    ]
    for k,v in enumerate(参数):
        r = s.countSegments(v[0])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))