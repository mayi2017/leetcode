'''
难度 简单
标签 字符串
题目链接 https://leetcode.cn/problems/ti-huan-kong-ge-lcof/description/
'''

from typing import List


class Solution:
    # 直接使用函数
    def pathEncryption1(self, path: str) -> str:

        return path.replace('.', ' ')
    
    # 遍历替换
    def pathEncryption2(self, path: str) -> str:
        s = ''
        for i in range(len(path)):
            if path[i] == '.':
                s += ' '
            else:
                s += path[i]

        return s
    
    # 分割 + 组合
    def pathEncryption(self, path: str) -> str:
        l = path.split('.')

        return ' ' . join(l)
 

if __name__ == '__main__':
    s = Solution()
    参数 = [
        ['a.aef.qerf.bb'],
    ]
    预期 = [
        "a aef qerf bb",
    ]
    for k,v in enumerate(参数):
        r = s.pathEncryption(v[0])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))