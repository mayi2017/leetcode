'''
难度 简单
题目链接 https://leetcode.cn/problems/number-of-senior-citizens/description/
'''

from typing import List

class Solution:
    def countSeniors1(self, details: List[str]) -> int:
        人数 = 0
        for d in details:
            年龄 = '' . join([d[11], d[12]])
            if int(年龄) > 60:
                人数 += 1

        return 人数

    # 学习一下 优雅简洁的写法1
    def countSeniors2(self, details: List[str]) -> int:

        return sum(1 for d in details if int(d[11:13]) > 60)
    
    # 学习一下 优雅简洁的写法2
    def countSeniors(self, details: List[str]) -> int:

        return sum(int(d[11:13]) > 60 for d in details)
    

if __name__ == '__main__':
    s = Solution()
    参数 = [
        [["7868190130M7522","5303914400F9211","9273338290F4010"]],
        [["1313579440F2036","2921522980M5644"]],
    ]
    预期 = [
        2,
        0,
    ]
    for k,v in enumerate(参数):
        r = s.countSeniors(v[0])
        if r != 预期[k]:
            print("第{}个case不通过 期望={} 结果={}\n".format(
                k+1,
                预期[k],
                r
            ))