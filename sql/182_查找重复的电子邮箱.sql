-- having
select p.email as Email
from Person p 
group by p.email having count(p.email) > 1;

-- 临时表
select p1.Email from (
    select p.email, count(p.email) as num
    from Person p 
    group by p.email
) p1
where p1.num > 1;

-- 自联表
select distinct p1.email as Email 
from Person p1 
left join Person p2 on p1.email = p2.email
where p1.id != p2.id;