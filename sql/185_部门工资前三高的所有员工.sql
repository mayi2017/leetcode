-- 难度：困难
-- https://leetcode.cn/problems/department-top-three-salaries/description/
-- 窗口函数 dense_rank 
select 
    d.name as Department,
    a.name as Employee,
    a.salary as Salary
from Department d 
left join (
    select 
        name,
        departmentId,
        salary,
        dense_rank() over (partition by departmentId order by salary desc) as rk
    from Employee 
) a on a.departmentId = d.id 
where a.rk <= 3;

-- 自联表
select 
    d.name as Department,
    e1.name as Employee,
    e1.salary as Salary
from Employee e1, Department d 
where (
    select 
        count(distinct e2.salary)
    from Employee e2
    where e1.salary < e2.salary and e1.DepartmentId = e2.DepartmentId
) < 3 and e1.DepartmentId = d.id;