-- 难度：中等
-- https://leetcode.cn/problems/rank-scores/description/
-- 窗口函数 dense_rank
select 
    s.score,
    dense_rank() over (
        order by s.score desc
    ) as 'rank'
from Scores s;

-- count distinct 子查询
select
  s1.score,
  (
    select
      count(distinct s2.score)
    from
      Scores s2
    where
      s2.score >= s1.score
  ) as 'rank'
from
  Scores s1
ORDER BY
  s1.score desc;

-- inner join, group by, order by, count, distinct 
select 
    s1.score,
    count(distinct s2.score) as 'rank'
from Scores s1 
inner join Scores s2 on s1.score <= s2.score
group by s1.id, s1.score
order by s1.score desc;