-- https://leetcode.cn/problems/number-of-unique-subjects-taught-by-each-teacher/description/
-- group by count
select 
    t.teacher_id, 
    count(distinct t.subject_id) as cnt 
from Teacher t 
group by t.teacher_id;