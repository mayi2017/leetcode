-- 难度：中等
-- https://leetcode.cn/problems/friend-requests-ii-who-has-the-most-friends/description/
-- 刚开始提交两次都错误了，源自于使用了union，而不是union all，当出现记录相同的时候会把多行记录覆盖，一定要注意这个知识点
select 
    a.id,
    sum(a.num) as num
from (
    select 
        r.accepter_id as id,
        count(r.accepter_id) as num
    from RequestAccepted r 
    group by r.accepter_id
    union all
    select 
        r.requester_id as id,
        count(r.requester_id) as num
    from RequestAccepted r 
    group by r.requester_id
) a 
group by a.id
order by sum(a.num) desc;