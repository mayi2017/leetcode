-- 难度：中等
-- https://leetcode.cn/problems/market-analysis-i/description/
-- 子查询
select 
    u.user_id as buyer_id,
    u.join_date,
    ifnull(a.orders_in_2019, 0) as orders_in_2019
from Users u 
left join (
    select 
        o.buyer_id,
        count(o.buyer_id) as orders_in_2019
    from Orders o 
    where o.order_date >= '2019-01-01' and o.order_date < '2020-01-01'
    group by o.buyer_id
) a on a.buyer_id = u.user_id;