-- https://leetcode.cn/problems/average-time-of-process-per-machine/description/
-- group by, sum, if, 子查询
select 
  a.machine_id,
  round(
    sum(if(a.activity_type = 'start', -a.timestamp, a.timestamp))
    /
    (select count(*) from Activity where activity_type='start' group by machine_id limit 1), 
    3
  ) as processing_time
from Activity a 
group by a.machine_id;