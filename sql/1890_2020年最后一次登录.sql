-- https://leetcode.cn/problems/the-latest-login-in-2020/description/
-- group by, max
select 
    l.user_id,
    max(l.time_stamp) as last_stamp
from Logins l 
where l.time_stamp >= '2020-01-01 00:00:00' and l.time_stamp < '2021-01-01 00:00:00'
group by l.user_id;