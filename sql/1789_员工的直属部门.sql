-- https://leetcode.cn/problems/primary-department-for-each-employee/description/
-- union, 子查询
select 
  distinct a.employee_id, 
  a.department_id
from (
  select e.employee_id, e.department_id
  from Employee e 
  where e.primary_flag = 'Y'
  union
  select e.employee_id, e.department_id
  from Employee e 
  group by e.employee_id having count(e.employee_id) = 1
) a;