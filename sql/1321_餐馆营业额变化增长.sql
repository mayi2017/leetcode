-- 难度：中等
-- https://leetcode.cn/problems/restaurant-growth/description/
-- 窗口函数 sum 滑动窗口
select 
    distinct b.visited_on,
    b.sum_amount as amount,
    round(b.sum_amount/7, 2) as average_amount
from (
    -- 当天+前六天的金额和
    select 
        a.visited_on,
        sum(a.amount) over (order by a.visited_on range between interval 6 day preceding and current row) as sum_amount
    from (
        -- 每天总金额
        select 
            c.visited_on,
            sum(c.amount) as amount
        from Customer c
        group by c.visited_on 
    ) a 
) b 
-- 之前第七天及其之后的日期
where datediff(b.visited_on, (select min(visited_on) from Customer)) >= 6;