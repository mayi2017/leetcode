-- 难度：中等
-- https://leetcode.cn/problems/tree-node/description/
-- 分别查出各节点记录，再用union组合起来
select 
    id,
    'Root' as type
from Tree 
where p_id is null
union 
select 
    id,
    'Leaf' as type
from Tree 
where p_id is not null and id not in (
    select p_id from Tree where p_id is not null
)
union
select 
    id,
    'Inner' as type
from Tree 
where p_id is not null and id in (
    select distinct p_id from Tree where p_id is not null
);

-- case when 
select 
    id,
    case 
        when t1.p_id is null then 'Root'
        when t1.id in (select distinct p_id from Tree where p_id is not null) then 'Inner'
        else 'Leaf'
    end as Type
from Tree t1;