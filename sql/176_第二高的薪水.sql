-- 难度：中等
-- https://leetcode.cn/problems/second-highest-salary/description/
-- 外面再加一层查询，为了解决记录不存在时要输出null
select (
    select 
        salary
    from Employee 
    group by salary
    order by salary desc 
    limit 1, 1
) as SecondHighestSalary;

-- distinct 
select (
    select 
        distinct salary
    from Employee 
    order by salary desc 
    limit 1, 1
) as SecondHighestSalary;

-- max < max
select 
    max(salary) as SecondHighestSalary
from Employee 
where salary < (
    select max(salary) from Employee
);

-- max != max 
select 
    max(salary) as SecondHighestSalary
from Employee 
where salary != (
    select max(salary) from Employee
);