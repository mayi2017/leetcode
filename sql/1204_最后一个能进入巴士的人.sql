-- 难度：中等
-- https://leetcode.cn/problems/last-person-to-fit-in-the-bus/description/
-- 滑动窗口 sum 计算体重和
select 
    a.person_name
from (
    select 
        person_name,
        sum(weight) over (rows between unbounded preceding and current row) as total_weight
    from Queue
    order by turn asc
) a 
where a.total_weight <= 1000
order by a.total_weight desc 
limit 1;