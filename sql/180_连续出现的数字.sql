-- 难度：中等
-- https://leetcode.cn/problems/consecutive-numbers/description/
-- 窗口函数 row_number
select 
    distinct a.num as ConsecutiveNums
from (
    select 
        l.*,
        row_number() over (partition by l.num order by l.id asc) as rownum,
        row_number() over (order by l.id asc) as id2
    from logs l
) a
group by (a.id2-a.rownum), a.num 
having count(*) >= 3;