-- https://leetcode.cn/problems/replace-employee-id-with-the-unique-identifier/description/
-- left join
select eu.unique_id, e.name
from Employees e 
left join EmployeeUNI eu on eu.id = e.id;