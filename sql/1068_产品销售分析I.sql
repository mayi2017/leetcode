-- https://leetcode.cn/problems/product-sales-analysis-i/description/
-- group by having
select p.product_name, s.year, s.price
from Sales s 
left join Product p on p.product_id = s.product_id;