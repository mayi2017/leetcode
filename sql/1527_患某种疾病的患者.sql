-- https://leetcode.cn/problems/patients-with-a-condition/description/
-- regexp
select p.patient_id, p.patient_name, p.conditions
from Patients p 
where p.conditions != '' and p.conditions regexp '\\bDIAB1.*';

-- or like 
select p.patient_id, p.patient_name, p.conditions
from Patients p 
where p.conditions != '' 
  and (p.conditions like 'DIAB1%' or p.conditions like '% DIAB1%');