-- left join 
select c.name as Customers
from Customers c 
left join Orders o on c.id = o.customerId
where o.id is null;

-- 子查询 exists
select c.name as Customers
from Customers c 
where not exists (
    select o.id from Orders o where c.id = o.customerId
);