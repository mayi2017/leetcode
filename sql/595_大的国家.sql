-- or
select w.name, w.population, w.area
from World w 
where w.area >= 3000000 or w.population >= 25000000;

-- union
select name, population, area from World where area >= 3000000
union
select name, population, area from World where population >= 25000000;