-- https://leetcode.cn/problems/recyclable-and-low-fat-products/description/
-- 直接查
select 
    p.product_id
from Products p 
where p.low_fats = 'Y' and p.recyclable = 'Y';