-- https://leetcode.cn/problems/triangle-judgement/description/
-- not in ifnull
select s.name
from SalesPerson s
where s.sales_id not in (
  select ifnull(o.sales_id, 0)
  from Company c 
  left join Orders o on o.com_id = c.com_id
  where c.name = 'RED'
);
