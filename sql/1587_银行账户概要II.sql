-- https://leetcode.cn/problems/bank-account-summary-ii/description/
-- group by sum
select u.name, sum(t.amount) as balance
from Transactions t 
left join Users u on u.account = t.account 
group by t.account having sum(t.amount) > 10000;