-- 难度：中等
-- https://leetcode.cn/problems/product-sales-analysis-iii/description/
-- 先查询第一年，在连表
select 
    a.product_id,
    a.year as first_year,
    s2.quantity,
    s2.price
from (
    select 
        s1.product_id,
        min(s1.year) as year
    from Sales s1 
    group by s1.product_id
) a 
inner join Sales s2 on s2.product_id = a.product_id and s2.year = a.year;

-- 使用窗口函数 rank
select 
    a.product_id,
    a.year as first_year,
    a.quantity,
    a.price
from (
    select 
        s1.*,
        rank() over (partition by product_id order by year asc) as num
    from Sales s1 
) a 
where a.num = 1;