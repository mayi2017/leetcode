-- mod
select c.* 
from cinema c 
where mod(c.id, 2) = 1 and c.description != 'boring'
order by c.rating desc;

-- %
select c.* 
from cinema c 
where c.id % 2 = 1 and c.description != 'boring'
order by c.rating desc;

-- &
select c.* 
from cinema c 
where c.id & 1 = 1 and c.description != 'boring'
order by c.rating desc;