-- 难度：困难
-- https://leetcode.cn/problems/trips-and-users/description/
-- avg 算百分比
select 
    t.request_at as Day,
    round(
        avg(if(t.status != 'completed', 1, 0))
        ,2
    ) as 'Cancellation Rate'
from Trips t 
inner join Users u1 on u1.users_id = t.client_id
inner join Users u2 on u2.users_id = t.driver_id
where u1.banned = 'No' 
    and u2.banned = 'No'
    and t.request_at between '2013-10-01' and '2013-10-03'
group by t.request_at;