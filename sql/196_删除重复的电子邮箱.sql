-- left join
delete p1
from Person p1
left join Person p2 on p1.email = p2.email
where p1.id > p2.id;

-- 连表
delete p1 
from Person p1, Person p2
where p1.email = p2.email and p1.id > p2.id;

-- 子查询
delete from Person where id not in (
    select id from (
        select min(id) as id from Person group by email
    ) p
);