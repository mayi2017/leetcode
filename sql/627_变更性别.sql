-- if
update Salary set sex = if(sex = 'm', 'f', 'm');

-- case when
update Salary set sex = (case when sex = 'm' then 'f' when sex = 'f' then 'm' end);