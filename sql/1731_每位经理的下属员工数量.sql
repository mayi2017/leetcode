-- https://leetcode.cn/problems/the-number-of-employees-which-report-to-each-employee/description/
-- group by, order by, count, avg
select 
    e2.employee_id, 
    e2.name, 
    count(e1.employee_id) as reports_count,
    round(avg(e1.age), 0) as average_age
from Employees e1 
left join Employees e2 on e1.reports_to = e2.employee_id
where e1.reports_to >= 0
group by e1.reports_to
order by e2.employee_id asc;