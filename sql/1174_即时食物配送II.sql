-- 难度：中等
-- https://leetcode.cn/problems/immediate-food-delivery-ii/description/
-- 窗口函数 rank
select 
    -- 利用平均数得到概率
    round(
        avg(
            if(a.customer_pref_delivery_date > a.order_date, 0, 1)
        ) * 100, 
        2
    ) as immediate_percentage
from (
    -- 得到下单的日期排序
    select 
        order_date,
        customer_pref_delivery_date,
        rank() over (partition by customer_id order by order_date asc) num
    from Delivery 
) a 
-- 只取首次订单数据
where a.num = 1;