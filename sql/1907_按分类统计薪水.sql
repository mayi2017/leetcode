-- 难度：中等
-- https://leetcode.cn/problems/count-salary-categories/description/
-- 直接 union
select
    'Low Salary' as category,
    count(*) as accounts_count
from Accounts
where income < 20000
union 
select
    'Average Salary' as category,
    count(*) as accounts_count
from Accounts
where income >= 20000 and income <= 50000
union 
select
    'High Salary' as category,
    count(*) as accounts_count
from Accounts
where income > 50000;

-- 使用临时表 减少多次查询
with tmp_account as (
    select
        sum(if(income < 20000, 1, 0)) as 'L',
        sum(if(income >= 20000 and income <= 50000, 1, 0)) as 'A',
        sum(if(income > 50000, 1, 0)) as 'H'
    from Accounts
)

select 
    'Low Salary' as category,
    L as accounts_count
from tmp_account
union 
select 
    'Average Salary' as category,
    A as accounts_count
from tmp_account
union
select 
    'High Salary' as category,
    H as accounts_count
from tmp_account;