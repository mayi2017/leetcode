-- 难度：中等
-- https://leetcode.cn/problems/movie-rating/description/
-- union all
(
    select 
        u.name as results
    from MovieRating mr 
    left join Users u on u.user_id = mr.user_id 
    group by mr.user_id
    order by count(mr.rating) desc, u.name asc
    limit 1
)
union all
(
    select 
        m.title as results
    from MovieRating mr 
    left join Movies m on m.movie_id = mr.movie_id 
    where mr.created_at >= '2020-02-01' and mr.created_at < '2020-03-01'
    group by mr.movie_id
    order by avg(mr.rating) desc, m.title asc
    limit 1
)