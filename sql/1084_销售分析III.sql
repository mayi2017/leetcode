-- exists, not exists
select p.product_id, p.product_name
from Product p 
where exists (
    select 1 from Sales s 
    where s.product_id = p.product_id and s.sale_date >= '2019-01-01' and s.sale_date <= '2019-03-31'
) and not exists (
    select 1 from Sales s 
    where s.product_id = p.product_id and (s.sale_date < '2019-01-01' or s.sale_date > '2019-03-31')
);