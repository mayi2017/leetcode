-- 难度：中等
-- https://leetcode.cn/problems/product-price-at-a-given-date/description/
-- 子查询解决
select 
    a.product_id,
    if(b.price is null, a.price, b.price) as price
from (
    -- 所有产品及其初始价格
    select 
        p.product_id,
        10 as price
    from Products p 
    group by p.product_id
) a 
left join (
    -- 2019-08-16日之前最后的修改价格
    select 
        p.product_id,
        p.new_price as price
    from Products p 
    where (p.product_id, p.change_date) in (
        select 
            product_id,
            max(change_date) as change_date
        from Products 
        where change_date <= '2019-08-16'
        group by product_id
    )
) b on b.product_id = a.product_id;