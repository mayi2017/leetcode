-- https://leetcode.cn/problems/triangle-judgement/description/
-- if
select 
    t.x, t.y, t.z,
    if(t.x+t.y > t.z and t.x+t.z > t.y and t.y+t.z > t.x, 'Yes', 'No') as triangle
from Triangle t;
