-- 难度：中等
-- https://leetcode.cn/problems/exchange-seats/description/
-- 多重if
select
    s1.id,
    if(
        s1.id % 2 = 1,
        if(
            exists (select s2.student from Seat s2 where s2.id = s1.id + 1),
            (select s2.student from Seat s2 where s2.id = s1.id + 1),
            s1.student
        ),
        (select student from Seat s3 where s3.id = s1.id - 1)
    ) as student
from Seat s1;