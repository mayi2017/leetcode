-- https://leetcode.cn/problems/find-users-with-valid-e-mails/description/
-- regexp
select u.user_id, u.name, u.mail
from Users u 
where u.mail regexp '^[A-Za-z][A-Za-z0-9_.-]*\\@leetcode\\.com$';