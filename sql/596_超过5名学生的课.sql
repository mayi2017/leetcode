-- group by having
select c.class 
from Courses c 
group by c.class having count(c.student) >= 5;

-- 子查询
select a.class
from (
  select c.class, count(c.student) student_num
  from Courses c 
  group by c.class
) a 
where a.student_num >= 5;