-- 难度：中等
-- https://leetcode.cn/problems/confirmation-rate/description/
-- 子查询
select 
    s.user_id,
    if(
        a.user_id is null, 
        0.00,
        round(a.confirmed_num/a.total_num, 2)
    ) as confirmation_rate
from Signups s 
left join (
    select 
        user_id, 
        sum(if(action = 'confirmed', 1, 0)) as confirmed_num,
        count(c.action) as total_num
    from Confirmations c 
    group by user_id
) a on a.user_id = s.user_id;