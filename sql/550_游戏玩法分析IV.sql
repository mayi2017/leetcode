-- 难度：中等
-- https://leetcode.cn/problems/game-play-analysis-iv/description/
-- 子查询 avg
select 
    round(avg(a2.event_date is not null), 2) as fraction
from (
    select 
        player_id,
        min(event_date) as login
    from Activity
    group by player_id
) a1 
left join Activity a2 on a1.player_id = a2.player_id and datediff(a2.event_date, a1.login) = 1;