-- https://leetcode.cn/problems/customer-who-visited-but-did-not-make-any-transactions/description/
-- 子查询
select a.customer_id, count(a.amount_sum) as count_no_trans
from (
    select 
        v.customer_id,
        sum(ifnull(t.amount, 0)) as amount_sum
    from Visits v 
    left join Transactions t on t.visit_id = v.visit_id
    group by v.customer_id, v.visit_id
) a 
where a.amount_sum = 0
group by a.customer_id;