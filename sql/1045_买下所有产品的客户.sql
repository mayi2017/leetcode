-- 难度：中等
-- https://leetcode.cn/problems/customers-who-bought-all-products/description/
-- group by, having
select 
    c.customer_id
from Customer c 
group by c.customer_id
having count(distinct c.product_key) = (select count(1) from Product);