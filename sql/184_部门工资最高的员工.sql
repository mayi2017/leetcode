-- 难度：中等
-- https://leetcode.cn/problems/department-highest-salary/description/
-- 子查询
select 
    d.name as Department,
    e1.name as Employee,
    e1.Salary
from Employee e1 
inner join (
    select 
        e.departmentId,
        max(e.salary) as Salary
    from Employee e 
    group by e.departmentId
) e2 on e2.departmentId = e1.departmentId and e2.Salary = e1.Salary 
inner join Department d on d.id = e2.departmentId;

-- in 
select 
    d.name as Department,
    e.name as Employee,
    Salary
from Employee e, Department d 
where e.departmentId = d.id and 
(e.departmentId, e.Salary) in (
    select departmentId, max(Salary)
    from Employee
    group by departmentId
);