-- 难度：困难
-- https://leetcode.cn/problems/human-traffic-of-stadium/description/
-- 临时表 + group_concat
with t as (
    select 
        group_concat(a.id) as ids
    from (
        select 
            s.id,
            id-rank() over (order by s.visit_date) as num
        from Stadium s 
        where s.people >= 100
    ) a 
    group by a.num 
    having count(a.num) >= 3
)

select 
    s1.*
from Stadium s1 
where find_in_set(s1.id, (select group_concat(t.ids) from t))
order by s1.id asc;