-- 难度：中等
-- https://leetcode.cn/problems/managers-with-at-least-5-direct-reports/description/
-- 子查询 group by, having
select 
    e2.name
from (
    select 
        e.managerId
    from Employee e 
    where e.managerId is not null
    group by e.managerId 
    having count(e.managerId) >= 5
) e1 
inner join Employee e2 on e2.id = e1.managerId;