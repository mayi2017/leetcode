-- 难度：中等
-- https://leetcode.cn/problems/capital-gainloss/description/
-- group by, sum, if
select 
    s.stock_name,
    sum(if(s.operation = 'Buy', -s.price, s.price)) as 'capital_gain_loss'
from Stocks s 
group by s.stock_name;