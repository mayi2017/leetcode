-- 难度：中等
-- https://leetcode.cn/problems/nth-highest-salary/description/
-- 
CREATE FUNCTION getNthHighestSalary(N INT) RETURNS INT
BEGIN
    declare b int default N - 1;
    if(b < 0) then 
        return null;
    else 
        return (
            select ifnull(
                (select distinct salary from Employee order by salary desc limit b, 1), null
            )
        );
    end if;
END