-- https://leetcode.cn/problems/employees-with-missing-information/description/
-- union, order by
select a.employee_id
from (
  select e.employee_id
  from Employees e 
  left join Salaries s on e.employee_id = s.employee_id
  where s.employee_id is null
  union 
  select s.employee_id
  from Salaries s 
  left join Employees e on e.employee_id = s.employee_id
  where e.employee_id is null
) a 
order by a.employee_id asc;