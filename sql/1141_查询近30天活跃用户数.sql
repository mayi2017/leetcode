-- group by
select a.activity_date as day, count(distinct a.user_id) as active_users
from Activity a 
where a.activity_date <= '2019-07-27' and a.activity_date >= date_sub('2019-07-27', interval 29 day)
group by a.activity_date;