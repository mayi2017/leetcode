-- left join
select w2.id 
from Weather w1 
left join Weather w2 on date_add(w1.recordDate, interval 1 day) = w2.recordDate
where w2.temperature > w1.temperature;

-- 连表
select w2.id 
from Weather w1, Weather w2
where date_add(w1.recordDate, interval 1 day) = w2.recordDate and w2.temperature > w1.temperature;

-- exists
select w1.id
from Weather w1 
where exists (
    select 1 from Weather w2 
    where datediff(w1.recordDate, w2.recordDate) = 1 and w1.temperature > w2.temperature
);