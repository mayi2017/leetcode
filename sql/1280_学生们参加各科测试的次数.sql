-- https://leetcode.cn/problems/students-and-examinations/description/
-- cross join 子查询
select s.student_id, s.student_name, ss.subject_name, ifnull(ee.exam_num, 0) as attended_exams
from Students s 
cross join Subjects ss
left join (
  select e.student_id, e.subject_name, count(e.student_id) as exam_num
  from Examinations e 
  group by e.student_id, e.subject_name
) ee on ee.student_id = s.student_id and ee.subject_name = ss.subject_name
group by s.student_id, ss.subject_name
order by s.student_id, ss.subject_name;