-- https://leetcode.cn/problems/fix-names-in-a-table/description/
-- 先取出首字符（left）大写（upper），截取首字符后的字符（substring）小写（lower），最后连接（concat）
select 
    u.user_id,
    concat(
        upper(left(u.name, 1)),
        lower(substring(u.name, 2))
    ) as name
from Users u
order by u.user_id asc;