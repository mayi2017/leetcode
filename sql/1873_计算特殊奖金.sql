-- https://leetcode.cn/problems/calculate-special-bonus/description/
-- if, order by
select 
    e.employee_id,
    if(e.employee_id % 2 = 1 and left(e.name, 1) != 'M', e.salary, 0) as bonus
from Employees e
order by employee_id asc;