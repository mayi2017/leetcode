-- https://leetcode.cn/problems/list-the-products-ordered-in-a-period/description/
-- left join group by sum
select p.product_name, sum(o.unit) as unit
from Orders o 
left join Products p on p.product_id = o.product_id 
where o.order_date >= '2020-02-01' and o.order_date < '2020-03-01'
group by o.product_id having sum(o.unit) >= 100;