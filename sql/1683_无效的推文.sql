-- https://leetcode.cn/problems/invalid-tweets/description/
-- length
select t.tweet_id
from Tweets t 
where length(t.content) > 15;