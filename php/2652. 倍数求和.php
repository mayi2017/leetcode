<?php
/**
 * 难度 简单
 * 题目链接 https://leetcode.cn/problems/sum-multiples/description/
 */

class Solution {

    /**
     * 挨个遍历
     * @param int $n
     * @return int
     */
    public static function sumOfMultiples1(int $n): int
    {
        $结果 = 0;
        for ($i=1; $i <= $n; $i++) { 
            if ($i % 3 == 0 || $i % 5 == 0 || $i % 7 == 0) {
                $结果 += $i;
            }
        }

        return $结果;
    }

    /**
     * 3个遍历
     * @param int $n
     * @return int
     */
    public static function sumOfMultiples(int $n): int
    {
        $结果 = [];
        for ($i=3; $i <= $n; $i+=3) { 
            $结果[] = $i;
        }
        for ($i=5; $i <= $n; $i+=5) { 
            $结果[] = $i;
        }
        for ($i=7; $i <= $n; $i+=7) { 
            $结果[] = $i;
        }

        return array_sum(array_unique($结果));
    }
}

$test = [
    [7],
    [10],
    [9],
];
$result = [
    21,
    40,
    30,
];

foreach ($test as $key => $value) {
    $r = Solution::sumOfMultiples($value[0]);
    if ($r !== $result[$key]) {
        echo sprintf("第%d个case不通过", $key) . PHP_EOL;
    }
    echo $r . PHP_EOL;
}