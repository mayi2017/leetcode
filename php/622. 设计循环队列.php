<?php
/**
 * 难度 简单
 * 题目链接 https://leetcode.cn/problems/sum-multiples/description/
 */

class MyCircularQueue 
{
    /** @var array 数组 模拟队列 */
    private $arr = [];

    /** @var int 队列的容量 比数组的长度少一个 */
    private $队列长度 = 0;
    /** @var int 数组的长度 */
    private $数组长度 = 0;

    /** @var int 指向队列头部第 1 个有效数据的位置 */
    private $头 = 0;
    /** @var int 指向队列尾部（即最后 1 个有效数据）的下一个位置，即下一个从队尾入队元素的位置。 */
    private $尾 = 0;

    /**
     * @param int $k
     */
    function __construct($k) 
    {
        $this->队列长度 = $k;
        $this->数组长度 = $k + 1;
    }

    /**
     * 向循环队列插入一个元素。如果成功插入则返回真。
     * @param int $value
     * @return Boolean
     */
    function enQueue($value) 
    {
        if ($this->isFull()) {
            return false;
        }
        $this->arr[$this->尾] = $value;
        $this->尾 = ($this->尾 + 1) % $this->数组长度;

        return true;
    }

    /**
     * 从循环队列中删除一个元素。如果成功删除则返回真。
     * @return Boolean
     */
    function deQueue() 
    {
        if ($this->isEmpty()) {
            return false;
        }
        $this->头 = ($this->头 + 1) % $this->数组长度;

        return true;
    }

    /**
     * 从队首获取元素。如果队列为空，返回 -1 。
     * @return int
     */
    function Front() 
    {
        if ($this->isEmpty()) {
            return -1;
        }

        return $this->arr[$this->头];
    }

    /**
     * 获取队尾元素。如果队列为空，返回 -1
     * @return int
     */
    function Rear() 
    {
        if ($this->isEmpty()) {
            return -1;
        }

        return $this->arr[($this->尾 - 1 + $this->数组长度) % $this->数组长度];
    }

    /**
     * 检查循环队列是否为空
     * @return Boolean
     */
    function isEmpty() 
    {
        return $this->头 == $this->尾;
    }

    /**
     * 检查循环队列是否已满
     * 浪费一个空间 空出来 队空 和 队满 的判断条件不冲突
     * @return Boolean
     */
    function isFull() 
    {
        return ($this->尾 + 1) % $this->数组长度 == $this->头;
    }
}


$obj = new MyCircularQueue($k);
$ret_1 = $obj->enQueue($value);
$ret_2 = $obj->deQueue();
$ret_3 = $obj->Front();
$ret_4 = $obj->Rear();
$ret_5 = $obj->isEmpty();
$ret_6 = $obj->isFull();