<?php
/**
 * 难度 简单
 * 题目链接 https://leetcode.cn/problems/excel-sheet-column-title/description/
 */

class Solution {

    /** 
     * 还有问题   没有通过
     * @param int $columnNumber
     * @return String
     */
    public static function convertToTitle(int $columnNumber): string
    {
        $结果 = '';
        if ($columnNumber <= 0) {
            return '';
        }
        do {
            if ($columnNumber <= 26) {
                return self::获取数字对应的字符($columnNumber % 26) . $结果;
            }
            $余数 = $columnNumber % 26;
            $余数 == 0 && $columnNumber--;
            $columnNumber = floor($columnNumber / 26);
            $结果 = self::获取数字对应的字符($余数) . $结果;
            echo "余数=$余数 商=$columnNumber 结果=$结果 \n";
        } while ($columnNumber > 0);

        return $结果;
    }

    /**
     * @param int $columnNumber
     * @return String
     */
    public static function convertToTitle1(int $n): string
    {
        if($n<=0) return "";
        $s = "";
        while($n > 0){
            $n --;
            $s = chr(fmod($n,26) + 65).$s;
            $n = floor($n/26);
        }
        return $s;
    }

    public static function 获取数字对应的字符(int $num)
    {
        return chr(64 + $num);
    }
}

$test = [
    [1],
    [28],
    [701],
    [2147483647],
];
$result = [
    'A',
    'AB',
    'ZY',
    'FXSHRXW',
];

foreach ($test as $key => $value) {
    $r = Solution::convertToTitle($value[0]);
    if ($r !== $result[$key]) {
        echo sprintf("第%d个case不通过", $key) . PHP_EOL;
    }
    echo $r . PHP_EOL;
}