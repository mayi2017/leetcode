<?php
/**
 * 难度：中等
 * 题目链接 https://leetcode.cn/problems/single-number-ii/description/
 */

class Solution {

    /**
     * 利用isset
     * @param int[] $nums
     * @return int
     */
    public static function singleNumber1(array $nums): int 
    {
        $出现一次 = [];
        $出现多次 = [];
        foreach ($nums as $v) {
            if (isset($出现一次[$v]) || isset($出现多次[$v])) {
                unset($出现一次[$v]);
                $出现多次[$v] = 1;
            } else {
                $出现一次[$v] = 1;
            }
        }

        return array_keys($出现一次)[0];
    }

    /**
     * 利用题目条件的特性
     * @param int[] $nums
     * @return int
     */
    public static function singleNumber2(array $nums): int 
    {
        $去重数组 = array_unique($nums);
        
        return (array_sum($去重数组) * 3 - array_sum($nums)) / 2;
    }

    /**
     * array_search
     * @param int[] $nums
     * @return int
     */
    public static function singleNumber(array $nums): int 
    {

        return array_search(1, array_count_values($nums));
    }

}

$test = [
    [[2,2,3,2]],
    [[0,1,0,1,0,1,99]],
];
$result = [
    3,
    99,
];

foreach ($test as $key => $value) {
    $r = Solution::singleNumber($value[0]);
    if ($r !== $result[$key]) {
        echo sprintf("第%d个case不通过", $key) . PHP_EOL;
    }
    echo $r . PHP_EOL;
}