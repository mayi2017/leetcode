<?php
/**
 * 题目链接 https://leetcode.cn/problems/length-of-last-word/description/
 */

class Solution {

    /**
     * trim explode array_pop
     * @param String $s
     * @return Integer
     */
    public static function lengthOfLastWord(string $s): int 
    {
        $s = explode(' ', trim($s));
        $last = array_pop($s);
        
        return strlen($last);
    }
}

$test = [
    ["Hello World"],
    ["   fly me   to   the moon  "],
    ["luffy is still joyboy"],
];
$result = [
    5,
    4,
    6,
];

foreach ($test as $key => $value) {
    $r = Solution::lengthOfLastWord($value[0]);
    if ($r !== $result[$key]) {
        echo sprintf("第%d个case不通过", $key) . PHP_EOL;
    }
    echo $r . PHP_EOL;
}