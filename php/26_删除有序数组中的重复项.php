<?php
/**
 * 题目链接 https://leetcode.cn/problems/remove-duplicates-from-sorted-array/description/
 */

class Solution {

    /**
     * 用动态规划 赋值新数组
     * @param int[] $nums
     * @return int
     */
    public static function removeDuplicates1(array &$nums): int 
    {
        $len = count($nums);
        $pre = $nums[0];
        $result[] = $nums[0];

        for ($i=1; $i < $len; $i++) { 
            if ($pre != $nums[$i]) {
                $pre = $nums[$i];
                $result[] = $nums[$i];
            }
        }
        $nums = $result;
        
        return count($nums);
    }

    /**
     * 用动态规划 unset
     * @param int[] $nums
     * @return int
     */
    public static function removeDuplicates(array &$nums): int 
    {
        $len = 0;
        $pre = null;

        foreach ($nums as $key => $value) {
            if ($pre === $value) {
                unset($nums[$key]);
                continue;
            }
            $pre = $value;
            $len++;
        }
        
        
        return $len;
    }
}

$test = [
    [[1,1,2]],
    [[0,0,1,1,1,2,2,3,3,4]],
];
$result = [
    2,
    5,
];

foreach ($test as $key => $value) {
    $r = Solution::removeDuplicates($value[0]);
    if ($r !== $result[$key]) {
        echo sprintf("第%d个case不通过", $key) . PHP_EOL;
    }
    echo $r . PHP_EOL;
}