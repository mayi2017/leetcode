<?php
/**
 * 难度：中等
 * 题目链接 https://leetcode.cn/problems/single-number-iii/description/
 */

class Solution {

    /**
     * @param int[] $nums
     * @return int[]
     */
    public static function singleNumber($nums): array 
    {
        $长度 = count($nums);
        if ($长度 <= 2) {
            return $nums;
        }
        // 两个不重复数 a b 
        $a = $b = 0;
        $tmp = 0;
        foreach ($nums as $v) {
            // 所有的数都异或 因为相同的数异或结果是0 所以最终结果 tmp 为两个不重复的数a,b异或的结果
            $tmp ^= $v;
        }
        $diff = $tmp & -$tmp;
        echo sprintf("tmp=%d -tmp=%d \$tmp & -\$tmp=%d\n", $tmp, -$tmp, $diff);

        foreach ($nums as $v) {
            if ($diff & $v) {
                $a ^= $v;
            } else {
                $b ^= $v;
            }
        }

        return [$a, $b];
    }

    /**
     * @param int[] $nums
     * @return int[]
     */
    public static function singleNumber1($nums): array 
    {
        $结果 = [];
        $a = array_count_values($nums);
        foreach ($a as $k => $v) {
            if ($v == 1) {
                $结果[] = $k;
            }
        }

        return $结果;
    }
}

$test = [
    [[1,2,1,3,2,5]],
    // [[-1,0]],
    // [[0,1]],
];
$result = [
    [3,5],
    [-1,0],
    [0,1],
];

foreach ($test as $key => $value) {
    $r = Solution::singleNumber($value[0]);
    if ($r !== $result[$key]) {
        echo sprintf("第%d个case不通过", $key) . PHP_EOL;
    }
    print_r($r);
}