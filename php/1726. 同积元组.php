<?php
/**
 * 难度 中等
 * 题目链接 https://leetcode.cn/problems/tuple-with-same-product/description/
 */

class Solution {

    /**
     * @param int[] $nums
     * @return int
     */
    public static function tupleSameProduct(array $nums): int
    {
        $结果 = 0;
        $数组长度 = count($nums);
        /** @var array 用来记录相同乘积的元组对数 */
        $乘积映射 = [];
        for ($i=0; $i < $数组长度 - 1; $i++) { 
            for ($j= $i + 1; $j < $数组长度; $j++) {
                if (!isset($乘积映射[$nums[$i] * $nums[$j]])) {
                    $乘积映射[$nums[$i] * $nums[$j]] = 0;
                }
                $乘积映射[$nums[$i] * $nums[$j]]++;
            }
        }

        foreach ($乘积映射 as $k => $v) {
            // 观察用例可以发现，每两组组乘积相同的数据可以排列出8中不同的组合，因此最终的结果是 C(n,2) * 8
            if ($v > 1) {
                $结果 += $v * ($v - 1) / 2 * 8;
            }
        }

        return $结果;
    }
}

$test = [
    [[2,3,4,6]],
    [[1,2,4,5,10]],
];
$result = [
    8,
    16,
];

foreach ($test as $key => $value) {
    $r = Solution::tupleSameProduct($value[0]);
    if ($r !== $result[$key]) {
        echo sprintf("第%d个case不通过", $key) . PHP_EOL;
    }
    echo $r . PHP_EOL;
}