<?php
/**
 * 难度：简单
 * 题目链接 https://leetcode.cn/problems/summary-ranges/description/
 */

class Solution {

    /**
     * @param int[] $nums
     * @return String[]
     */
    public static function summaryRanges($nums): array 
    {
        $结果 = [];
        $长度 = count($nums);
        $开始 = $上一个数 = $nums[0];
        if ($长度 == 1) {
            $结果[] = strval($开始);
        }
        for ($i=1; $i < $长度; $i++) { 
            // 当前不连续
            if ($nums[$i] - $上一个数 != 1) {
                $结果[] = $上一个数 == $开始 ? strval($开始) : "$开始->$上一个数";
                $开始 = $上一个数 = $nums[$i];
            } else {
                // 还是连续
                $上一个数 = $nums[$i];
            }
            // 最后一个元素
            if ($i == $长度 - 1) {
                $结果[] = $上一个数 == $开始 ? strval($开始) : "$开始->$上一个数";
            }
        }

        return $结果;
    }
}

$test = [
    [[0,1,2,4,5,7]],
    [[0,2,3,4,6,8,9]],
    [[-1]],
];
$result = [
    ["0->2","4->5","7"],
    ["0","2->4","6","8->9"],
    ['-1'],
];

foreach ($test as $key => $value) {
    $r = Solution::summaryRanges($value[0]);
    if ($r !== $result[$key]) {
        echo sprintf("第%d个case不通过", $key) . PHP_EOL;
    }
    print_r($r);
}