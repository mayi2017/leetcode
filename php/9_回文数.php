<?php
/**
 * 题目链接 https://leetcode.cn/problems/palindrome-number/description/
 */

class Solution {

    /**
     * 使用strrev 字符串翻转
     * @param int $x
     * @return Boolean
     */
    public static function isPalindrome1($x): bool 
    {
        $x = strval($x);

        return $x == strrev($x);
    }

    /**
     * 首尾比较
     * @param int $x
     * @return Boolean
     */
    public static function isPalindrome($x): bool 
    {
        $x = strval($x);
        $len = strlen($x);
        $i = 0;

        while ($i < $len / 2) {
            if ($x[$i] !== $x[$len - $i - 1]) {
                return false;
            }
            $i++;
        }

        return true;
    }
}

$test = [
    [121, true],
    [-121, false],
    [10, false],
];

foreach ($test as $key => $value) {
    $result = Solution::isPalindrome($value[0]);
    var_dump($result);
}