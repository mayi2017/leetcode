<?php
/**
 * 难度：简单
 * 题目链接 https://leetcode.cn/problems/merge-sorted-array/description/
 */

class Solution {

    /**
     * @param int[] $nums1
     * @param int $m
     * @param int[] $nums2
     * @param int $n
     * @return NULL
     */
    public static function merge(&$nums1, $m, $nums2, $n): void 
    {
        $new = [];
        $nums1 = array_slice($nums1, 0, $m);
        $num1 = array_shift($nums1);
        $num2 = array_shift($nums2);

        for ($i=0; $i < $m+$n; $i++) { 
            if (is_null($num1)) {
                $new[] = $num2;
                $new = array_merge($new, $nums2);
                break;
            } elseif (is_null($num2)) {
                $new[] = $num1;
                $new = array_merge($new, $nums1);
                break;
            } else {
                if ($num1 <= $num2) {
                    $new[] = $num1;
                    $num1 = array_shift($nums1);
                } else {
                    $new[] = $num2;
                    $num2 = array_shift($nums2);
                }
            }
        }

        $nums1 = $new;
        // print_r($nums1);
    }
}

$test = [
    [[1,2,3,0,0,0], 3, [2,5,6], 3],
    [[1], 1, [], 0],
    [[0], 0, [1], 1],
];
$result = [
    [1,2,2,3,5,6],
    [1],
    [1],
];

foreach ($test as $key => $value) {
    Solution::merge($value[0], $value[1], $value[2], $value[3]);
}