<?php
/**
 * 难度 简单
 * 题目链接 https://leetcode.cn/problems/longest-common-prefix/description/
 */

class Solution {

    /**
     * 循环太多，超时了 【不通过】
     * @param String[] $strs
     * @return String
     */
    public static function longestCommonPrefix1(array $strs)
    {
        $公共前缀 = '';
        $下标 = 0;
        while (true) {
            $首个单词字符 = $strs[0][$下标];
            for ($i=1; $i < count($strs); $i++) { 
                // 停止条件
                if (!isset($strs[$i][$下标]) || $首个单词字符 != $strs[$i][$下标]) {
                    break 2;
                }
            }
            $公共前缀 .= $首个单词字符;
            $下标++;
        }

        return $公共前缀;
    }

    /**
     * 取出首个单词 与其他的相比 【通过】
     * @param String[] $strs
     * @return String
     */
    public static function longestCommonPrefix(array $strs)
    {
        $公共前缀 = '';
        $第一个单词 = array_shift($strs);
        for ($i=0; $i < strlen($第一个单词); $i++) { 
            for ($j=0; $j < count($strs); $j++) { 
                // 停止条件
                if (!isset($strs[$j][$i]) || $第一个单词[$i] != $strs[$j][$i]) {
                    break 2;
                }
            }
            $公共前缀 .= $第一个单词[$i];
        }

        return $公共前缀;
    }
}

$test = [
    [["flower","flow","flight"]],
    [["dog","racecar","car"]],
];
$result = [
    "fl",
    '',
];

foreach ($test as $key => $value) {
    $r = Solution::longestCommonPrefix($value[0]);
    if ($r !== $result[$key]) {
        echo sprintf("第%d个case不通过", $key) . PHP_EOL;
    }
    echo $r . PHP_EOL;
}