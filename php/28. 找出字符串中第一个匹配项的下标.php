<?php
/**
 * 难度 简单
 * 题目链接 https://leetcode.cn/problems/find-the-index-of-the-first-occurrence-in-a-string/description/
 */

class Solution {

    /**
     * @param String $haystack
     * @param String $needle
     * @return int
     */
    public static function strStr(string $haystack, string $needle): int
    {
        $第二个串得长度 = strlen($needle);
        for ($i=0; $i <= strlen($haystack) - $第二个串得长度; $i++) { 
            $截取串 = substr($haystack, $i, $第二个串得长度);
            if ($截取串 == $needle) {
                return $i;
            }
        }

        return -1;
    }
}

$test = [
    ["sadbutsad", "sad"],
    ["leetcode", "leeto"],
];
$result = [
    0,
    -1,
];

foreach ($test as $key => $value) {
    $r = Solution::strStr($value[0], $value[1]);
    if ($r !== $result[$key]) {
        echo sprintf("第%d个case不通过", $key) . PHP_EOL;
    }
    echo $r . PHP_EOL;
}