<?php
/**
 * 题目链接 https://leetcode.cn/problems/pascals-triangle/description/
 */

class Solution {

    /**
     * 利用递归来解决问题
     * @param Integer $numRows
     * @return Integer[][]
     */
    public static function generate1(int $numRows): array 
    {
        $result = [];
        for ($i=1; $i <= $numRows; $i++) { 
            $result[] = self::getRow($i);
        }

        return $result;
    }

    public static function getRow(int $numRows): array
    {
        if ($numRows == 1) {
            return [1];
        } elseif ($numRows == 2) {
            return [1, 1];
        } else {
            $result = [1];
            $pre = self::getRow($numRows - 1);
            for ($i=1; $i < count($pre); $i++) { 
                $result[] = $pre[$i - 1] + $pre[$i];
            }
        }
        $result[] = 1;

        return $result;
    }

    /**
     * 两层for来解决
     * @param Integer $numRows
     * @return Integer[][]
     */
    public static function generate(int $numRows): array 
    {
        $result = [];
        for ($i=0; $i < $numRows; $i++) { 
            if ($i == 0) {
                $result[$i][] = 1;
            } else {
                // 第一个元素
                $result[$i][] = 1;
                for ($j=1; $j < $i; $j++) { 
                    $result[$i][] = $result[$i-1][$j-1] + $result[$i-1][$j];
                }
                // 最后一个元素
                $result[$i][] = 1;
            }
            
        }

        return $result;
    }

}

$test = [
    [5],
    [1],
    [7],
];
$result = [
    [[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]],
    [[1]],
    [[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1],[1,5,10,10,5,1],[1,6,15,20,15,6,1]],
];

foreach ($test as $key => $value) {
    $r = Solution::generate($value[0]);
    if ($r !== $result[$key]) {
        echo sprintf("第%d个case不通过", $key) . PHP_EOL;
    }
    print_r($r);
}