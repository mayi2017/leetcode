<?php
/**
 * 难度 简单
 * 题目链接 https://leetcode.cn/problems/binary-tree-paths/description/
 */

class TreeNode {
    public $val = null;
    public $left = null;
    public $right = null;
    function __construct($val = 0, $left = null, $right = null) {
        $this->val = $val;
        $this->left = $left;
        $this->right = $right;
    }
}

class Solution {

    /**
     * @param TreeNode $root
     * @return String[]
     */
    public static function binaryTreePaths1($root): array
    {
        $结果 = [];
        for ($i=count($root)-1; $i >= 0; $i--) { 
            if (self::是否叶子节点($root, $i)) {
                $结果[] = self::找叶子节点上面的路径($root, $i);
            }
        }

        return $结果;
    }

    public static function 找叶子节点上面的路径($root, $当前下标)
    {
        $路径 = [];
        while ($当前下标 > 0) {
            array_unshift($路径, $root[$当前下标]);
            $当前下标 = floor(($当前下标 + 1) / 2) - 1;
        }
        array_unshift($路径, $root[0]);

        return implode('->', $路径);
    }

    public static function 是否叶子节点($root, $当前下标)
    {
        return isset($root[$当前下标]) && 
            !isset($root[pow(2, $当前下标 + 1) - 1]) && 
            !isset($root[pow(2, $当前下标 + 1)]);
    }

    /**
     * @param TreeNode $root
     * @return String[]
     */
    public static function binaryTreePaths($root): array
    {
        $结果 = [];
        

        return $结果;
    }
}

$test = [
    [[1,2,3,null,5]],
    [[1]],
];
$result = [
    ["1->2->5","1->3"],
    ["1"],
];

foreach ($test as $key => $value) {
    $r = Solution::binaryTreePaths($value[0]);
    if ($r !== $result[$key]) {
        echo sprintf("第%d个case不通过", $key) . PHP_EOL;
    }
    print_r($r);
}