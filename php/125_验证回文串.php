<?php
/**
 * 题目链接 https://leetcode.cn/problems/valid-palindrome/description/
 */

class Solution {

    /**
     * 利用首尾比较
     * @param String $s
     * @return Boolean
     */
    public static function isPalindrome(string $s): bool 
    {
        $s = preg_replace("/[^A-Za-z0-9]+/", '', $s);
        $s = strtolower($s);

        $len = strlen($s);
        $i = 0;
        $j = $len - 1;
        
        if ($len <= 1) {
            return true;
        }
        while ($i < $len / 2) {
            if ($s[$i] != $s[$j]) {
                return false;
            }
            $i++;
            $j--;
        }

        return true;
    }

}

$test = [
    ["A man, a plan, a canal: Panama"],
    ["race a car"],
    [" "],
];
$result = [
    true, false, true,
];

foreach ($test as $key => $value) {
    $r = Solution::isPalindrome($value[0]);
    if ($r !== $result[$key]) {
        echo sprintf("第%d个case不通过", $key) . PHP_EOL;
    }
    var_dump($r);
}