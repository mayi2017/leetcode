<?php
/**
 * 难度 简单
 * 题目链接 https://leetcode.cn/problems/add-strings/description/
 */

class Solution {

    /**
     * @param String $num1
     * @param String $num2
     * @return String
     */
    public static function addStrings(string $num1, string $num2): string
    {
        // 字符串倒置
        $num1 = strrev($num1);
        $num2 = strrev($num2);
        $进位 = 0;
        $最大长度 = max(strlen($num1), strlen($num2));
        $最后结果 = '';
        for ($i=0; $i < $最大长度; $i++) { 
            $和 = ($num1[$i] ?? 0) + ($num2[$i] ?? 0) + $进位;
            $最后结果 .= strval($和 % 10);
            $进位 = intval($和 / 10);
        }
        if ($进位 > 0) {
            $最后结果 .= '1';
        }

        return strrev($最后结果);
    }
}

$test = [
    ["11", "123"],
    ["456", "77"],
    ["0", "0"],
];
$result = [
    '134',
    '533',
    '0',
];

foreach ($test as $key => $value) {
    $r = Solution::addStrings($value[0], $value[1]);
    if ($r !== $result[$key]) {
        echo sprintf("第%d个case不通过", $key) . PHP_EOL;
    }
    echo $r . PHP_EOL;
}