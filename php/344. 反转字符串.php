<?php
/**
 * 难度 简单
 * 题目链接 https://leetcode.cn/problems/reverse-string/description/
 */

class Solution {

    /**
     * @param String[] $s
     * @return NULL
     */
    public static function reverseString(array &$s): void
    {
        $数组长度 = count($s);
        for ($i=0; $i < floor($数组长度 / 2); $i++) { 
            list($s[$i], $s[$数组长度 - 1 - $i]) = [$s[$数组长度 - 1 - $i], $s[$i]];
        }
        // print_r($s);
    }
}

$test = [
    [["h","e","l","l","o"]],
    [["H","a","n","n","a","h"]],
];
$result = [
    ["o","l","l","e","h"],
    ["h","a","n","n","a","H"],
];

foreach ($test as $key => $value) {
    Solution::reverseString($value[0]);
}