<?php
/**
 * 难度：简单
 * 题目链接 https://leetcode.cn/problems/contains-duplicate/description/
 */

class Solution {

    /**
     * @param int[] $nums
     * @return Boolean
     */
    public static function containsDuplicate1($nums): bool 
    {
        $map = [];
        foreach ($nums as $v) {
            if (isset($map[$v])) {
                return true;
            }
            $map[$v] = 1;
        }

        return false;
    }

    /**
     * @param int[] $nums
     * @return Boolean
     */
    public static function containsDuplicate($nums): bool 
    {
        $map = array_unique($nums);
        
        return $map != $nums;
    }
}

$test = [
    [[1,2,3,1]],
    [[1,2,3,4]],
    [[1,1,1,3,3,4,3,2,4,2]],
];
$result = [
    true,
    false,
    true,
];

foreach ($test as $key => $value) {
    $r = Solution::containsDuplicate($value[0]);
    if ($r !== $result[$key]) {
        echo sprintf("第%d个case不通过", $key) . PHP_EOL;
    }
    echo $r . PHP_EOL;
}