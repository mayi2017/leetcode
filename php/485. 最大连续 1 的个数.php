<?php
/**
 * 难度：简单
 * 题目链接 https://leetcode.cn/problems/max-consecutive-ones/description/
 */

class Solution {

    /**
     * @param int[] $nums
     * @return int
     */
    public static function findMaxConsecutiveOnes($nums): int 
    {
        $最大数量 = 0;
        $连续数量 = 0;
        foreach ($nums as $v) {
            if ($v == 0) {
                $连续数量 = 0;
            } else {
                $连续数量++;
                $最大数量 = $连续数量 > $最大数量 ? $连续数量 : $最大数量;
            }
        }

        return $最大数量;
    }
}

$test = [
    [[1,1,0,1,1,1]],
    [[1,0,1,1,0,1]],
];
$result = [
    3,
    2,
];

foreach ($test as $key => $value) {
    $r = Solution::findMaxConsecutiveOnes($value[0]);
    if ($r !== $result[$key]) {
        echo sprintf("第%d个case不通过", $key) . PHP_EOL;
    }
    echo $r . PHP_EOL;
}