<?php
/**
 * 难度 简单
 * 题目链接 https://leetcode.cn/problems/first-unique-character-in-a-string/description/
 */

class Solution {
    
    /**
     * @param String $s
     * @return Integer
     */
    public static function firstUniqChar(string $s): int
    {
        $元素统计 = array_count_values(str_split($s));

        for ($i=0; $i < strlen($s); $i++) { 
            if ($元素统计[$s[$i]] == 1) {
                return $i;
            }
        }

        return -1;
    }
}

$test = [
    ['leetcode'],
    ['loveleetcode'],
    ['aabb'],
];
$result = [
    0,
    2,
    -1,
];

foreach ($test as $key => $value) {
    $r = Solution::firstUniqChar($value[0]);
    if ($r !== $result[$key]) {
        echo sprintf("第%d个case不通过", $key) . PHP_EOL;
    }
    echo $r . PHP_EOL;
}