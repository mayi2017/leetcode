<?php
/**
 * 难度 简单
 * 题目链接 https://leetcode.cn/problems/detect-capital/description/
 */

class Solution {
    
    /**
     * 完全用函数解决
     * @param String $word
     * @return Boolean
     */
    public static function detectCapitalUse1(string $word): bool
    {
        $大写 = strtoupper($word);
        $小写 = strtolower($word);
        $首字母大写 = ucfirst($小写);

        return $大写 == $word || $小写 == $word || $首字母大写 == $word;
    }

    /**
     * 自己用ASCII去比对
     * @param String $word
     * @return Boolean
     */
    public static function detectCapitalUse(string $word): bool
    {
        // 第一个字母大写 后面可以全大写或者全小写 第一个字母小写 后面必须全小写
        $字符区间 = [];
        $数组 = str_split($word);
        $首字母 = array_shift($数组);
        if (ord($首字母) >= 97 && ord($首字母) <= 122) {
            $字符区间 = [97, 122];
        }
        
        foreach ($数组 as $k => $v) {
            if ($k == 0 && empty($字符区间)) {
                $字符区间 = (ord($v) >= 65 && ord($v) <= 90) ? [65, 90] : [97, 122];
            } else {
                if (ord($v) < $字符区间[0] || ord($v) > $字符区间[1]) {
                    return false;
                }
            }
        }

        return true;
    }
}

$test = [
    ["USA"],
    ["FlaG"],
];
$result = [
    true,
    false,
];

foreach ($test as $key => $value) {
    $r = Solution::detectCapitalUse($value[0]);
    if ($r !== $result[$key]) {
        echo sprintf("第%d个case不通过", $key) . PHP_EOL;
    }
    echo $r . PHP_EOL;
}