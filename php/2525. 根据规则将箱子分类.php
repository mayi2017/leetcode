<?php
/**
 * 难度 简单
 * 题目链接 https://leetcode.cn/problems/categorize-box-according-to-criteria/description/
 */

class Solution {

    /**
     * @param int $length
     * @param int $width
     * @param int $height
     * @param int $mass
     * @return String
     */
    public static function categorizeBox($length, $width, $height, $mass): string
    {
        /** @var array 利用二进制特性 第一位表示 Bulky 第二位表示 Heavy 得到一个映射 */
        $结果类型映射 = [
            0 => 'Neither',
            1 => 'Heavy',
            2 => 'Bulky',
            3 => 'Both',
        ];
        $基础类型 = 0;
        if (
            $length >= pow(10, 4) 
            || $width >= pow(10, 4) 
            || $height >= pow(10, 4) 
            || $length * $width * $height >= pow(10, 9)) {
            $基础类型 += 2;
        }
        if ($mass >= 100) {
            $基础类型 += 1;
        }

        return $结果类型映射[$基础类型];
    }
}

$test = [
    [1000, 35, 700, 300],
    [200, 50, 800, 50],
    [10000, 1, 1, 1],
];
$result = [
    "Heavy",
    "Neither",
    "Bulky",
];

foreach ($test as $key => $value) {
    $r = Solution::categorizeBox($value[0], $value[1], $value[2], $value[3]);
    if ($r !== $result[$key]) {
        echo sprintf("第%d个case不通过", $key) . PHP_EOL;
    }
    echo $r . PHP_EOL;
}