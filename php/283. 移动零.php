<?php
/**
 * 难度：简单
 * 题目链接 https://leetcode.cn/problems/move-zeroes/description/
 */

class Solution {

    /**
     * 双指针解决
     * @param int[] $nums
     * @return NULL
     */
    public static function moveZeroes1(&$nums): void 
    {
        $慢指针 = 0;
        for ($快指针=0; $快指针 < count($nums); $快指针++) { 
            if ($nums[$快指针] != 0) {
                list($nums[$慢指针], $nums[$快指针]) = [$nums[$快指针], $nums[$慢指针]];
                $慢指针++;
            }
        }
        // print_r($nums);
    }

    /**
     * unset
     * @param int[] $nums
     * @return NULL
     */
    public static function moveZeroes(&$nums): void 
    {
        foreach ($nums as $k => $v) {
            if ($v == 0) {
                unset($nums[$k]);
                $nums[] = 0;
            }
        }
        
        print_r($nums);
    }
}

$test = [
    [[0,1,0,3,12]],
    [[0]],
];
$result = [
    [1,3,12,0,0],
    [0],
];

foreach ($test as $key => $value) {
    Solution::moveZeroes($value[0]);
}