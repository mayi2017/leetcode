<?php
/**
 * 难度：简单
 * 题目链接 https://leetcode.cn/problems/contains-duplicate-ii/description/
 */

class Solution {

    /**
     * @param int[] $nums
     * @param int $k
     * @return Boolean
     */
    public static function containsNearbyDuplicate($nums, $k): bool 
    {
        $map = [];
        foreach ($nums as $k1 => $v1) {
            if (isset($map[$v1]) && ($k1 - $map[$v1] <= $k)) {
                return true;
            }
            $map[$v1] = $k1;
        }
        
        return false;
    }
}

$test = [
    [[1,2,3,1], 3],
    [[1,0,1,1], 1],
    [[1,2,3,1,2,3], 2],
];
$result = [
    true,
    true,
    false,
];

foreach ($test as $key => $value) {
    $r = Solution::containsNearbyDuplicate($value[0], $value[1]);
    if ($r !== $result[$key]) {
        echo sprintf("第%d个case不通过", $key) . PHP_EOL;
    }
    echo $r . PHP_EOL;
}