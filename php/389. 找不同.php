<?php
/**
 * 难度 简单
 * 题目链接 https://leetcode.cn/problems/find-the-difference/description/
 */

class Solution {

    /**
     * @param String $s
     * @param String $t
     * @return String
     */
    public static function findTheDifference1(string $s, string $t): string
    {
        // 将 t 的 kv 翻转 并统计其个数
        $t_map = [];
        for ($i=0; $i < strlen($t); $i++) { 
            $t_map[$t[$i]] = $t_map[$t[$i]] ?? 0;
            $t_map[$t[$i]] += 1;
        }
        // 将 s 的 kv 翻转 并统计其个数
        $s_map = [];
        for ($i=0; $i < strlen($s); $i++) { 
            $s_map[$s[$i]] = $s_map[$s[$i]] ?? 0;
            $s_map[$s[$i]] += 1;
        }
        foreach ($t_map as $k => $v) {
            // 不存在或者数量不相同 都是答案
            if (!isset($s_map[$k]) || $s_map[$k] != $v) {
                return $k;
            }
        }

        return '';
    }

    /**
     * @param String $s
     * @param String $t
     * @return String
     */
    public static function findTheDifference(string $s, string $t): string
    {
        $ascii_sum_t = 0;
        for ($i=0; $i < strlen($t); $i++) { 
            $ascii_sum_t += ord($t[$i]);
        }
        $ascii_sum_s = 0;
        for ($i=0; $i < strlen($s); $i++) { 
            $ascii_sum_s += ord($s[$i]);
        }

        return chr($ascii_sum_t - $ascii_sum_s);
    }
}

$test = [
    ["abcd", "abcde"],
    ["", "y"],
];
$result = [
    'e',
    'y',
];

foreach ($test as $key => $value) {
    $r = Solution::findTheDifference($value[0], $value[1]);
    if ($r !== $result[$key]) {
        echo sprintf("第%d个case不通过", $key) . PHP_EOL;
    }
    echo $r . PHP_EOL;
}