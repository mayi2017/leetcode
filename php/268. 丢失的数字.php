<?php
/**
 * 难度：简单
 * 题目链接 https://leetcode.cn/problems/missing-number/description/
 */

class Solution {

    /**
     * @param int[] $nums
     * @return int
     */
    public static function missingNumber($nums): int 
    {
        $n = count($nums);

        return (1+$n)*$n/2 - array_sum($nums);
    }
}

$test = [
    [[3,0,1]],
    [[0,1]],
    [[9,6,4,2,3,5,7,0,1]],
    [[0]],
];
$result = [
    2,
    2,
    8,
    1,
];

foreach ($test as $key => $value) {
    $r = Solution::missingNumber($value[0]);
    if ($r !== $result[$key]) {
        echo sprintf("第%d个case不通过", $key) . PHP_EOL;
    }
    echo $r . PHP_EOL;
}