<?php
/**
 * 难度 简单
 * 题目链接 https://leetcode.cn/problems/word-pattern/description/
 */

class Solution {

    /**
     * @param String $pattern
     * @param String $s
     * @return Boolean
     */
    public static function wordPattern(string $pattern, string $s): bool
    {
        $第二个字符串数组 = explode(' ', $s);
        // 数量不一致
        if (strlen($pattern) != count($第二个字符串数组)) {
            return false;
        }
        // 不重复数量不一致
        if (count(array_unique(str_split($pattern))) != count(array_unique($第二个字符串数组))) {
            return false;
        }

        $临时映射 = [];
        $第一个字符串映射结果 = [];
        for ($i=0; $i < strlen($pattern); $i++) { 
            if (!isset($临时映射[$pattern[$i]])) {
                $临时映射[$pattern[$i]] = $第二个字符串数组[$i];
            }
            $第一个字符串映射结果[] = $临时映射[$pattern[$i]];
        }

        return $第一个字符串映射结果 === $第二个字符串数组;
    }
}

$test = [
    ["abba", "dog cat cat dog"],
    ["abba", "dog cat cat fish"],
    ["aaaa", "dog cat cat dog"],
];
$result = [
    true,
    false,
    false,
];

foreach ($test as $key => $value) {
    $r = Solution::wordPattern($value[0], $value[1]);
    if ($r !== $result[$key]) {
        echo sprintf("第%d个case不通过", $key) . PHP_EOL;
    }
    echo $r . PHP_EOL;
}