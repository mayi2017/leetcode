<?php
/**
 * 难度 简单
 * 题目链接 https://leetcode.cn/problems/reverse-vowels-of-a-string/description/
 */

class Solution {
    public static $元音集合 = ['a','e','i','o','u','A','E','I','O','U'];
    
    /**
     * @param String $s
     * @return String
     */
    public static function reverseVowels(string $s): string
    {
        $数组长度 = strlen($s);
        for ($i=0, $j=$数组长度-1; $i < $数组长度; $i++) { 
            // 停止条件
            if ($i >= $j) {
                break;
            }
            // 一直找到左侧元音
            if (!in_array($s[$i], self::$元音集合)) {
                continue;
            }
            // 开始找右侧元音
            $j = self::寻找右侧元音位置($j, $s);

            list($s[$i], $s[$j]) = [$s[$j], $s[$i]];
            $j--;
        }
        
        return $s;
    }

    public static function 寻找右侧元音位置(int $j, string $s)
    {
        for ($i=$j; $i >= 0; $i--) { 
            // 一直找到左侧元音
            if (in_array($s[$i], self::$元音集合)) {
                return $i;
            }
        }

        return 0;
    }
}

$test = [
    ['hello'],
    ['leetcode'],
];
$result = [
    'holle',
    'leotcede',
];

foreach ($test as $key => $value) {
    $r = Solution::reverseVowels($value[0]);
    if ($r !== $result[$key]) {
        echo sprintf("第%d个case不通过", $key) . PHP_EOL;
    }
    echo $r . PHP_EOL;
}