<?php
/**
 * 题目链接 https://leetcode.cn/problems/single-number/description/
 */

class Solution {

    /**
     * 利用isset
     * @param int[] $nums
     * @return int
     */
    public static function singleNumber1(array $nums): int 
    {
        $result[$nums[0]] = $nums[0];
        for ($i=1; $i < count($nums); $i++) { 
            if (isset($result[$nums[$i]])) {
                unset($result[$nums[$i]]);
            } else {
                $result[$nums[$i]] = $nums[$i];
            }
        }

        return array_keys($result)[0];
    }

    /**
     * 利用异或运算
     * @param int[] $nums
     * @return int
     */
    public static function singleNumber(array $nums): int 
    {
        // 成双成对的数 异或之后结果都为0 只剩下单独的数字 就是结果
        for ($i=0, $res=0; $i < count($nums); $i++) { 
            $res ^= $nums[$i];
        }

        return $res;
    }
}

$test = [
    [[2,2,1]],
    [[4,1,2,1,2]],
    [[1]],
];
$result = [
    1,
    4,
    1,
];

foreach ($test as $key => $value) {
    $r = Solution::singleNumber($value[0]);
    if ($r !== $result[$key]) {
        echo sprintf("第%d个case不通过", $key) . PHP_EOL;
    }
    echo $r . PHP_EOL;
}