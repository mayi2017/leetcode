file='file.txt'
num=$(cat ${file} | wc -l)
# echo $num

if [ $num -lt 10 ];then
    echo ''
else
    # -n: 这是一个选项，告诉 sed 不打印每一行的输出，除非明确要求。
    sed -n "10p" $file
fi