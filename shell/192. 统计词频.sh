# tr 用于转换或删除文件中的字符 -s
# sort 排序单词
# uniq 命令用于检查及删除文本文件中重复出现的行列，一般与 sort 命令结合使用。 -c：在每列旁边显示该行重复出现的次数。
# sort -r 倒序排序 -n 选项表示按数值进行排序，而不是默认的按字符进行排序。
# awk 按照格式输出
cat words.txt | tr -s ' ' '\n' | sort | uniq -c | sort -nr | awk '{print $2, $1}'