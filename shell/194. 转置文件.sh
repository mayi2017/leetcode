# 求出列数
num=$(head -1 file.txt | wc -w)
# 循环每列横向输出
for i in $(seq 1 $num)
do
# awk '{print $'$i'}' file.txt | xargs
awk -v col=$i '{print $col}' file.txt | xargs
done